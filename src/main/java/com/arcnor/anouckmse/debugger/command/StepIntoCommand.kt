package com.arcnor.anouckmse.debugger.command

import com.arcnor.anouckmse.debugger.Debugger
import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog

class StepIntoCommand(console: ConsoleDialog) : StepCommand(console, "si", "Step into", Debugger.ACT_STEP_INTO)
