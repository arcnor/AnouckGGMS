package com.arcnor.cpu.gameboy

import com.arcnor.anouckmse.machine.gameboy.GBDebug
import com.arcnor.cpu.debugger.JumpType
import com.arcnor.cpu.debugger.OpcodeInfo
import com.arcnor.cpu.debugger.ROMDebug
import com.arcnor.cpu.intel8080.Intel8080Debug

class LR35902Debug(private val machineDebug: GBDebug, private val romDebug: ROMDebug?) : Intel8080Debug<LR35902>() {
	override fun decodeOpcodeInfo(opcode: Int): OpcodeInfo {
		opcodeInfo.opcodeSize = 1
		opcodeInfo.dataSize = 0

		if (opcode in 0x40..0xBF) return opcodeInfo

		when (opcode) {
			LR35902.OP_BIT_REG -> opcodeInfo.opcodeSize++
			else -> {
				val bit07 = opcode and 0b00000111
				val bit0F = opcode and 0b00001111
				val bitF0 = opcode and 0b11110000
				when {
					bit07 == 0x06 -> opcodeInfo.dataSize = 1
					bit07 in 0x02..0x04 && bitF0 in 0xC0..0xD0 -> opcodeInfo.dataSize = 2
					bit07 == 0x00 && (bitF0 in 0x10..0x30 || bitF0 in 0xE0..0xF0) -> opcodeInfo.dataSize = 1
					bit0F == 0x01 && (bitF0 in 0x00..0x30) -> opcodeInfo.dataSize = 2
					opcode == 0x08 -> opcodeInfo.dataSize = 2
					opcode == 0xCD -> opcodeInfo.dataSize = 2
					opcode == 0xEA || opcode == 0xFA -> opcodeInfo.dataSize = 2
				}
			}
		}

		return opcodeInfo
	}

	override fun opcodeJumpType(opcode: Int, data: Int): JumpType {
		val baseOpcode = opcode and 0xFF

		val iExtd = opcode and 0xDF00 == 0xDD00
		if (iExtd) {
			if (baseOpcode == 0xE9) {   // jp (i[xy])
				return JumpType.JUMP_UNKNOWN
			}
			return JumpType.NO_JUMP
		}
		// Normal
		when (opcode and 0xC7) {
			0xC0 -> return JumpType.RET_COND        // ret cond
			0xC2 -> return JumpType.JUMP_COND       // jp cond, **
			0xC4 -> return JumpType.CALL_COND       // call cond, **
			0xC7 -> return JumpType.CALL            // rst **
		}
		when (opcode) {
			0x18 -> return JumpType.JUMP_REL            // jr *
			0x10 -> return JumpType.JUMP_REL_COND       // djnz *
			0x20, 0x30, 0x28, 0x38 -> return JumpType.JUMP_REL_COND              // jr cond, **
			0x76 -> return JumpType.HALT
			0xC3 -> return JumpType.JUMP                // jp *
			0xC9 -> return JumpType.RET
			0xCD -> return JumpType.CALL
			0xE9 -> return JumpType.JUMP_UNKNOWN    // jp (hl)
		}
		return JumpType.NO_JUMP
	}

	override fun opcodeJumpData(opcode: Int, data: Int) = data

	override fun opToStr(oldPC: Int, opcode: Int, op: Int, op2: Int, data: Int, cpu: LR35902): String {
		var opName = "UNKNOWN"
		var comment = ""

		return String.format("%1$-20s; %2\$s", opName, comment)
	}

	override fun formatLabel(addr: Int): String {
		var lbl: String? = romDebug?.formatLabel(addr)
		if (lbl == null) {
			lbl = machineDebug.formatLabel(addr)
		}
		return if (lbl != null) lbl else String.format("L%1$04X", addr)
	}
}