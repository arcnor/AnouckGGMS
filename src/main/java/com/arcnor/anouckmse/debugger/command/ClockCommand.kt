package com.arcnor.anouckmse.debugger.command

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog
import java.awt.event.ActionEvent
import javax.swing.AbstractAction

class ClockCommand(console: ConsoleDialog) : Command(console, "clock", "Show CPU clock cycles", object : AbstractAction("Clock Command") {
	override fun actionPerformed(e: ActionEvent) {
		val cycles = console.debugger.cpu.clockCycles
		console.append("Clock: $cycles cycles\n")
	}
})
