package com.arcnor.cpu.z80

import com.arcnor.cpu.Flags

class Z80Alu(private val F: Flags) {
	fun xor(a: Int, b: Int): Int {
		val result = a xor b

		F[Z80Flags.FLAG_SIGN] = result < 0
		F[Z80Flags.FLAG_ZERO] = result == 0
		F[Z80Flags.FLAG_HALFCARRY] = false
		F[Z80Flags.FLAG_NEG] = false
		F[Z80Flags.FLAG_CARRY] = false
		F[Z80Flags.FLAG_PARITY] = false        // FIXME
		F[Z80Flags.FLAG_X] = b and 0x04 == 0    // FIXME
		F[Z80Flags.FLAG_Y] = b and 0x10 == 0    // FIXME

		return result
	}

	fun or(a: Int, b: Int): Int {
		// FIXME?
		val result = a or b

		F[Z80Flags.FLAG_SIGN] = result < 0
		F[Z80Flags.FLAG_ZERO] = result == 0
		F[Z80Flags.FLAG_HALFCARRY] = false
		F[Z80Flags.FLAG_NEG] = false
		F[Z80Flags.FLAG_CARRY] = false
		F[Z80Flags.FLAG_PARITY] = false        // FIXME
		F[Z80Flags.FLAG_X] = b and 0x04 == 0    // FIXME
		F[Z80Flags.FLAG_Y] = b and 0x10 == 0    // FIXME

		return result
	}
}
