package com.arcnor.anouckmse

import com.arcnor.anouckmse.debugger.Debugger
import com.arcnor.anouckmse.machine.c64.C64Debug
import com.arcnor.anouckmse.machine.chip8.CHIP8MachineDebug
import com.arcnor.anouckmse.machine.chip8.CHIP8Memory
import com.arcnor.anouckmse.machine.gameboy.GBDebug
import com.arcnor.anouckmse.machine.gameboy.GBMemory
import com.arcnor.anouckmse.machine.nes.NESMemory
import com.arcnor.anouckmse.machine.sms.SMSMemory
import com.arcnor.anouckmse.swing.Theming
import com.arcnor.cpu.chip8.CHIP8CPU
import com.arcnor.cpu.chip8.CHIP8CPUDebug
import com.arcnor.cpu.debugger.ROMDebug
import com.arcnor.cpu.gameboy.LR35902
import com.arcnor.cpu.gameboy.LR35902Debug
import com.arcnor.cpu.mos6502.MOS6502
import com.arcnor.cpu.mos6502.MOS6502Debug
import com.arcnor.cpu.z80.Z80
import com.arcnor.cpu.z80.Z80Debug
import com.arcnor.system.VideoChip
import com.arcnor.vdp.CHIP8Graphics
import com.arcnor.vdp.DMGGraphics
import com.arcnor.vdp.TMS9918
import java.io.File
import java.io.IOException

object Main {
	enum class Machine {
		NES, CHIP8, GB, SMS
	}

	@Throws(IOException::class)
	@JvmStatic fun main(args: Array<String>) {
		if (args.size < 2) {
			System.err.println("Usage: AnouckGGMS <machine> <rom>\n")
			System.err.println("Known machines:")
			Machine.values().forEach { System.err.println("\t$it") }
			return
		}

		val machine: Machine
		try {
			machine = Machine.valueOf(args[0].trim().toUpperCase())
		} catch (e: Exception) {
			throw UnsupportedOperationException("Invalid machine: ${args[0]}")
		}

		Theming.loadLAF()

		val f = File(args[1])
		if (!f.exists()) {
			throw UnsupportedOperationException("ROM doesn't exist: ${args[1]}")
		}
		val buf = f.readBytes()

		val romDebug = object : ROMDebug {
			override fun formatLabel(addr: Int) = when (addr) {
				0xC72D -> "BranchTests"
				0xC7DB -> "FlagTests"
				0xC885 -> "ImmediateTests"
				0xCBDE -> "ImpliedTests"
				0xCDF8 -> "StackTests"
				0xCEEE -> "AccumulatorTests"
				0xCFA2 -> "(I,X)_Tests"
				0xD174 -> "0P_Tests"
				0xD4FB -> "Abs_Tests"
				0xD900 -> "(I),Y_Tests"
				0xDAE0 -> "(I),Y_Tests2"
				0xDF4A -> "Abs,Y_Tests"
				0xDBB8 -> "0P,X_Tests"
				0xE1AA -> "Abs,X_Tests"
				0xC6A3 -> "InvalidNOPTests"
				0xE51E -> "InvalidLAXTests"
				0xE73D -> "InvalidSAXTests"
				0xE8D3 -> "InvalidSBCTests"
				0xE916 -> "InvalidDCPTests"
				0xEB86 -> "InvalidISBTests"
				0xEDF6 -> "InvalidSLOTests"
				0xF066 -> "InvalidRLATests"
				0xF2D6 -> "InvalidSRETests"
				0xF546 -> "InvalidRRATests"
				0xC66B -> "TestsOK"
				0xC66F -> "TestsNOT_OK"
				else -> null
			}

			override fun formatReadAddr(addr: Int) = null

			override fun formatWriteAddr(addr: Int) = null

			override fun commentReadAddr(addr: Int) = null

			override fun commentWriteAddr(addr: Int) = null
		}

		val debugger = when (machine) {
			Machine.SMS -> {
				val memChip = SMSMemory(buf)
				Debugger.create(Z80(memChip), TMS9918(memChip), Z80Debug())
			}
			Machine.GB -> Debugger.create(LR35902(GBMemory(buf)), DMGGraphics(), LR35902Debug(GBDebug(), romDebug))
			Machine.NES -> Debugger.create(MOS6502(NESMemory(buf)), object : VideoChip(0, 0) {}, MOS6502Debug(C64Debug(), romDebug))
			Machine.CHIP8 -> {
				val vdp = CHIP8Graphics()
				Debugger.create(CHIP8CPU(CHIP8Memory(buf), vdp), vdp, CHIP8CPUDebug(CHIP8MachineDebug(), romDebug))
			}
			else -> throw UnsupportedOperationException("Invalid machine")
		}
		debugger.isVisible = true
	}
}
