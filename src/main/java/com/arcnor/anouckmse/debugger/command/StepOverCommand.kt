package com.arcnor.anouckmse.debugger.command

import com.arcnor.anouckmse.debugger.Debugger
import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog

class StepOverCommand(console: ConsoleDialog) : StepCommand(console, "s", "Step over", Debugger.ACT_STEP_OVER)
