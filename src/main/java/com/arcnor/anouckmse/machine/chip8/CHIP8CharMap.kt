package com.arcnor.anouckmse.machine.chip8

internal val CHIP8CharMap = byteArrayOf(
		// "0"	Binary	Hex
		0xF0.toByte(),
		0x90.toByte(),
		0x90.toByte(),
		0x90.toByte(),
		0xF0.toByte(),
		// "1"	Binary	Hex
		0x20.toByte(),
		0x60.toByte(),
		0x20.toByte(),
		0x20.toByte(),
		0x70.toByte(),
		// "2"	Binary	Hex
		0xF0.toByte(),
		0x10.toByte(),
		0xF0.toByte(),
		0x80.toByte(),
		0xF0.toByte(),
		// "3"	Binary	Hex
		0xF0.toByte(),
		0x10.toByte(),
		0xF0.toByte(),
		0x10.toByte(),
		0xF0.toByte(),
		// "4"	Binary	Hex
		0x90.toByte(),
		0x90.toByte(),
		0xF0.toByte(),
		0x10.toByte(),
		0x10.toByte(),
		// "5"	Binary	Hex
		0xF0.toByte(),
		0x80.toByte(),
		0xF0.toByte(),
		0x10.toByte(),
		0xF0.toByte(),
		// "6"	Binary	Hex
		0xF0.toByte(),
		0x80.toByte(),
		0xF0.toByte(),
		0x90.toByte(),
		0xF0.toByte(),
		// "7"	Binary	Hex
		0xF0.toByte(),
		0x10.toByte(),
		0x20.toByte(),
		0x40.toByte(),
		0x40.toByte(),
		// "8"	Binary	Hex
		0xF0.toByte(),
		0x90.toByte(),
		0xF0.toByte(),
		0x90.toByte(),
		0xF0.toByte(),
		// "9"	Binary	Hex
		0xF0.toByte(),
		0x90.toByte(),
		0xF0.toByte(),
		0x10.toByte(),
		0xF0.toByte(),
		// "A"	Binary	Hex
		0xF0.toByte(),
		0x90.toByte(),
		0xF0.toByte(),
		0x90.toByte(),
		0x90.toByte(),
		// "B"	Binary	Hex
		0xE0.toByte(),
		0x90.toByte(),
		0xE0.toByte(),
		0x90.toByte(),
		0xE0.toByte(),
		// "C"	Binary	Hex
		0xF0.toByte(),
		0x80.toByte(),
		0x80.toByte(),
		0x80.toByte(),
		0xF0.toByte(),
		// "D"	Binary	Hex
		0xE0.toByte(),
		0x90.toByte(),
		0x90.toByte(),
		0x90.toByte(),
		0xE0.toByte(),
		// "E"	Binary	Hex
		0xF0.toByte(),
		0x80.toByte(),
		0xF0.toByte(),
		0x80.toByte(),
		0xF0.toByte(),
		// "F"	Binary	Hex
		0xF0.toByte(),
		0x80.toByte(),
		0xF0.toByte(),
		0x80.toByte(),
		0x80.toByte()
)