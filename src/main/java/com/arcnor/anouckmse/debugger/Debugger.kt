package com.arcnor.anouckmse.debugger

import com.arcnor.anouckmse.debugger.dialog.BreakpointsDialog
import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog
import com.arcnor.anouckmse.debugger.dialog.DebugDialog
import com.arcnor.anouckmse.debugger.dialog.DisplayDialog
import com.arcnor.anouckmse.debugger.dialog.MemoryDialog
import com.arcnor.anouckmse.swing.MutableListModel
import com.arcnor.anouckmse.swing.Theming
import com.arcnor.cpu.Flags
import com.arcnor.cpu.Register
import com.arcnor.cpu.debugger.CPUDebug
import com.arcnor.system.CPUChip
import com.arcnor.system.VideoChip
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Component
import java.awt.Dimension
import java.awt.FlowLayout
import java.awt.Rectangle
import java.awt.event.ActionEvent
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import java.awt.event.InputEvent
import java.awt.event.KeyEvent
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.util.HashMap
import java.util.LinkedHashSet
import java.util.concurrent.atomic.AtomicBoolean
import javax.swing.AbstractAction
import javax.swing.Action
import javax.swing.BorderFactory
import javax.swing.Icon
import javax.swing.ImageIcon
import javax.swing.JCheckBoxMenuItem
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JMenu
import javax.swing.JMenuBar
import javax.swing.JMenuItem
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.JPopupMenu
import javax.swing.JScrollBar
import javax.swing.JScrollPane
import javax.swing.JTable
import javax.swing.KeyStroke
import javax.swing.ListSelectionModel
import javax.swing.SwingWorker
import javax.swing.WindowConstants
import javax.swing.border.BevelBorder
import javax.swing.event.ListDataEvent
import javax.swing.event.ListDataListener
import javax.swing.event.PopupMenuEvent
import javax.swing.event.PopupMenuListener
import javax.swing.table.DefaultTableCellRenderer
import javax.swing.table.TableCellRenderer

@Suppress("NOTHING_TO_INLINE")
class Debugger private constructor(val cpu: CPUChip, val vdp: VideoChip, cpuDebug: CPUDebug<*>) : JFrame("AnouckGGMS Debugger") {
	companion object {
		const val ACT_STEP_INTO = "stepInto"
		const val ACT_STEP_OVER = "stepOver"
		const val ACT_STEP_OUT = "stepOut"
		const val ACT_CONTINUE = "continue"

		val COLOR_BACK_SELECTION = Color(66, 68, 69)
		val COLOR_BACK_BREAKPOINT = Color(135, 56, 45)
		val COLOR_BACK_CURRENT = Color(49, 63, 127)
		val COLOR_BACK_GUTTER = Color(49, 51, 53)
		val COLOR_FORE_REGCHANGED = Color.RED

		// TODO: This was created just because Kotlin doesn't allow generics on constructors only
		fun <C : CPUChip, V: VideoChip> create(cpu: C, vdp: V, cpuDebug: CPUDebug<C>) = Debugger(cpu, vdp, cpuDebug)
	}

	private val onStepActions = HashMap<String, Action>()
	private val breakpointListModel = BreakpointListModel<ASMTableModel.BreakpointPosition>()

	private val debugDialogs = LinkedHashSet<DebugDialog>()
	private val asmTableModel = ASMTableModel<CPUChip>(cpu, cpuDebug as CPUDebug<CPUChip>?, breakpointListModel)
	private val tblDisasm = JTable(asmTableModel)

	private val RUNNING = AtomicBoolean(false)

	private val actStepInto = object : AbstractAction("Step Into") {
		override fun actionPerformed(e: ActionEvent) {
			if (!RUNNING.get()) {
				cpu.doCycle()
				onStepGraphical()
			}
			RUNNING.set(false)
		}
	}

	private val actStepOver = object : AbstractAction("Step Over") {
		override fun actionPerformed(e: ActionEvent) {
			// FIXME
			cpu.doCycle()
			RUNNING.set(false)
			onStepGraphical()
		}
	}

	private val actStepOut = object : AbstractAction("Step Out") {
		override fun actionPerformed(e: ActionEvent) {
			// FIXME
			cpu.doCycle()
			RUNNING.set(false)
			onStepGraphical()
		}
	}

	private val actContinue = object : AbstractAction("Continue") {
		override fun actionPerformed(e: ActionEvent) {
			if (!RUNNING.get()) {
				RUNNING.set(true)
				val sw = object : SwingWorker<Any, Any>() {
					@Throws(Exception::class)
					override fun doInBackground(): Any? {
						try {
							do {
								cpu.doCycle()
								onStep()
							} while (!breakpointListModel.contains(ASMTableModel.BreakpointPosition(asmTableModel.addr2index(cpu.pc))) && RUNNING.get())
							RUNNING.set(false)
						} catch (ex: Exception) {
							ex.printStackTrace()
						}
						return null
					}

					override fun done() {
						onStepGraphical()      // A previous onStep() has been done before, no big deal
					}
				}
				sw.execute()
			}
		}
	}

	private val actLineToByte = object : AbstractAction("Convert to byte") {
		override fun actionPerformed(e: ActionEvent) {
			//To change body of implemented methods use File | Settings | File Templates.
		}
	}

	private val actLineToWord = object : AbstractAction("Convert to word") {
		override fun actionPerformed(e: ActionEvent) {
			//To change body of implemented methods use File | Settings | File Templates.
		}
	}

	private val actLineToString = object : AbstractAction("Convert to string") {
		override fun actionPerformed(e: ActionEvent) {
			//To change body of implemented methods use File | Settings | File Templates.
		}
	}

	private val debugKeyStrokes = LinkedHashSet<KeyStroke>()

	private val actUpdateView = object : AbstractAction("Update View") {
		override fun actionPerformed(e: ActionEvent?) {
			// FIXME: We refresh the whole table right now, because we don't know what the old PC is (to repaint it as well)
			tblDisasm.repaint()
			gotoLine(asmTableModel.addr2index(cpu.pc))
		}
	}

	init {
		background = Theming.COLOR_BACK_WINDOW
		foreground = Theming.COLOR_FORE

		defaultCloseOperation = WindowConstants.DISPOSE_ON_CLOSE
		layout = BorderLayout()

		tblDisasm.intercellSpacing = Dimension(0, 0)
		tblDisasm.foreground = foreground
		tblDisasm.background = background
		tblDisasm.selectionBackground = COLOR_BACK_SELECTION
		tblDisasm.selectionForeground = foreground
		tblDisasm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION)
		tblDisasm.setShowGrid(false)
		tblDisasm.tableHeader = null
		val popup = createDisasmPopup()
		//tblDisasm.setComponentPopupMenu();

		tblDisasm.setDefaultRenderer(Any::class.java, SimpleRenderer())
		tblDisasm.setDefaultRenderer(java.lang.Boolean::class.java, BreakpointRenderer())

		tblDisasm.addMouseListener(object : MouseAdapter() {
			override fun mouseClicked(e: MouseEvent?) {
				val tbl = e!!.source as JTable
				val col = tbl.columnAtPoint(e.point)
				if (col == 0) {
					val row = tbl.rowAtPoint(e.point)
					val pos = ASMTableModel.BreakpointPosition(row)
					if (breakpointListModel.contains(pos)) {
						breakpointListModel.remove(pos)
					} else {
						breakpointListModel.add(pos)
					}
				}
			}

			override fun mouseReleased(e: MouseEvent?) {
				if (e!!.isPopupTrigger || e.button == MouseEvent.BUTTON3) {
					val tbl = e.source as JTable
					val row = tbl.rowAtPoint(e.point)
					val column = tbl.columnAtPoint(e.point)

					if (!tbl.isRowSelected(row)) {
						tbl.changeSelection(row, column, false, false)
					}

					popup.show(e.component, e.x, e.y)
				}
			}
		})
		onStepActions.put("UpdateView", actUpdateView)

		breakpointListModel.addListDataListener(object : ListDataListener {
			override fun intervalAdded(e: ListDataEvent) {
				val model = e.source as MutableListModel<ASMTableModel.BreakpointPosition>
				tblDisasm.setValueAt(java.lang.Boolean.TRUE, model.getElementAt(e.index0).position, 0)
			}

			override fun intervalRemoved(e: ListDataEvent) {
				// FIXME: For the moment, we redraw the whole table as we don't know which index got removed
				tblDisasm.repaint()
			}

			override fun contentsChanged(e: ListDataEvent) {
				//To change body of implemented methods use File | Settings | File Templates.
			}
		})

		val columnModel = tblDisasm.columnModel
		columnModel.getColumn(ASMTableModel.COL_BREAKPOINT).maxWidth = 20
		columnModel.getColumn(ASMTableModel.COL_LABEL).maxWidth = 60
		columnModel.getColumn(ASMTableModel.COL_DATA).maxWidth = 60
		columnModel.getColumn(ASMTableModel.COL_OPCODE).maxWidth = 260
		val scrollPane = JScrollPane(tblDisasm)
		val disasmScrollBar = VisualRegionScroller(JScrollBar.HORIZONTAL, asmTableModel, scrollPane.verticalScrollBar)
		add(disasmScrollBar, BorderLayout.NORTH)
		add(scrollPane, BorderLayout.CENTER)
		val pnlInput = JPanel()
		pnlInput.background = Theming.COLOR_BACK_MENU
		pnlInput.layout = FlowLayout()
		for (j in 0..cpu.numRegisters - 1) {
			val reg = cpu.getRegister(j)
			pnlInput.add(createRegLabel(reg))
		}
		for (j in 0..cpu.numCounters - 1) {
			pnlInput.add(createIntRegLabel(j))
		}
		pnlInput.add(createFlagsLabel(cpu.flags))
		add(pnlInput, BorderLayout.SOUTH)
		setSize(800, 600)

		setLocationRelativeTo(null)

		disasmScrollBar.border = Theming.BORDER_TOP
		scrollPane.border = BorderFactory.createEmptyBorder()
		pnlInput.border = Theming.BORDER_TOP

		updateActionMap()

		createDialogs()
		createMenuBar()

		gotoLine(asmTableModel.addr2index(cpu.porAddr.toInt()))
	}

	private fun createDialogs() {
		debugDialogs.add(ConsoleDialog(this))
		debugDialogs.add(MemoryDialog(this, cpu.memory))
		debugDialogs.add(BreakpointsDialog(this, breakpointListModel))
		debugDialogs.add(DisplayDialog(this, vdp, width / vdp.width))

		for (dialog in debugDialogs) {
			dialog.isVisible = true
		}
	}

	private fun createMenuItem(act: Action, keyStroke: KeyStroke): JMenuItem {
		val result = JMenuItem(act)
		result.accelerator = keyStroke
		return result
	}

	private fun createDisasmPopup(): JPopupMenu {
		val result = JPopupMenu()
		val keyStrokes = arrayOf(KeyStroke.getKeyStroke('b'), KeyStroke.getKeyStroke('w'), KeyStroke.getKeyStroke('s'))
		val actions = arrayOf<Action>(actLineToByte, actLineToWord, actLineToString)

		for (j in keyStrokes.indices) {
			result.add(createMenuItem(actions[j], keyStrokes[j]))
		}

		result.addPopupMenuListener(object : PopupMenuListener {
			override fun popupMenuWillBecomeVisible(e: PopupMenuEvent) {
				val source = e.source as JPopupMenu
				val r = tblDisasm.selectedRow
				if (r < 0) {
					for (action in actions) {
						action.isEnabled = false
					}
				} else {
					for (action in actions) {
						action.isEnabled = true
					}
					val asmLine = asmTableModel.getASMLine(r)
					if (asmLine.opcode < 0) {
						when (asmLine.opcode) {
							ASMTableModel.DATA_BYTE -> actLineToByte.isEnabled = false
							ASMTableModel.DATA_WORD -> actLineToWord.isEnabled = false
							ASMTableModel.DATA_STRING -> actLineToString.isEnabled = false
						}
					}
				}
			}

			override fun popupMenuWillBecomeInvisible(e: PopupMenuEvent) {
				//To change body of implemented methods use File | Settings | File Templates.
			}

			override fun popupMenuCanceled(e: PopupMenuEvent) {
				//To change body of implemented methods use File | Settings | File Templates.
			}
		})
		return result
	}

	private fun createMenuBar() {
		val menubar = JMenuBar()
		jMenuBar = menubar
		menubar.border = BorderFactory.createEmptyBorder()

		val mnuDebug = JMenu("Debug")
		menubar.add(mnuDebug)
		mnuDebug.setMnemonic('D')

		val contentPane = contentPane as JPanel
		val inputMap = contentPane.inputMap
		val actionMap = contentPane.actionMap
		for (keyStroke in debugKeyStrokes) {
			val key = inputMap.get(keyStroke)
			val act = actionMap.get(key)
			mnuDebug.add(createMenuItem(act, keyStroke))
		}

		val mnuWindow = JMenu("Window")
		menubar.add(mnuWindow)
		mnuWindow.setMnemonic('W')

		val actShowDialog = object : AbstractAction("Show Dialog") {
			override fun actionPerformed(e: ActionEvent) {
				val item = e.source as JCheckBoxMenuItem
				val dialog = menubar.getClientProperty(item) as DebugDialog
				dialog.isVisible = !dialog.isVisible
			}
		}
		debugDialogs.withIndex().forEach { (idx, dialog) ->
			val item = JCheckBoxMenuItem(actShowDialog)
			dialog.addComponentListener(object : ComponentAdapter() {
				override fun componentShown(e: ComponentEvent?) {
					item.isSelected = true
				}

				override fun componentHidden(e: ComponentEvent?) {
					item.isSelected = false
				}
			})
			item.accelerator = KeyStroke.getKeyStroke(KeyEvent.VK_0 + idx, InputEvent.CTRL_MASK)
			item.isSelected = dialog.isVisible
			menubar.putClientProperty(item, dialog)
			item.text = dialog.title
			mnuWindow.add(item)
		}
	}

	private fun updateActionMap() {
		val contentPane = contentPane as JPanel

		val inputMap = contentPane.inputMap
		val actionMap = contentPane.actionMap

		val keyStrokes = arrayOf(KeyStroke.getKeyStroke("F8"), KeyStroke.getKeyStroke("F7"), KeyStroke.getKeyStroke(KeyEvent.VK_F8, InputEvent.SHIFT_MASK), KeyStroke.getKeyStroke("F9"))
		val keys = arrayOf(ACT_STEP_OVER, ACT_STEP_INTO, ACT_STEP_OUT, ACT_CONTINUE)
		val actions = arrayOf<Action>(actStepOver, actStepInto, actStepOut, actContinue)

		for (j in keyStrokes.indices) {
			inputMap.put(keyStrokes[j], keys[j])
			actionMap.put(keys[j], actions[j])
			debugKeyStrokes.add(keyStrokes[j])
		}
	}

	private fun createFlagsLabel(f: Flags): JPanel {
		val result = JPanel(FlowLayout())
		result.border = BorderFactory.createBevelBorder(BevelBorder.LOWERED)

		val updateFlagsAction = object : AbstractAction("Flags") {
			private val flags = BooleanArray(8)

			override fun actionPerformed(e: ActionEvent?) {
				for (j in 7 downTo 0) {
					val lbl = result.getComponent(7 - j) as JLabel
					val flagSet = f.get(j)
					if (flags[j] != flagSet) {
						if (e == null || e.actionCommand != "manual") {
							flags[j] = flagSet
						}

						lbl.foreground = COLOR_FORE_REGCHANGED
					} else {
						lbl.foreground = foreground
					}
					val fName = lbl.text
					lbl.text = if (flagSet) fName.toUpperCase() else fName.toLowerCase()
				}
				// Update flags as byte
				val lbl = result.getComponent(8) as JLabel
				lbl.text = String.format("(%1$02X)", f.get())
			}
		}
		onStepActions.put("F", updateFlagsAction)

		val lblMouseListener = object : MouseAdapter() {
			override fun mouseClicked(e: MouseEvent?) {
				val lbl = e!!.source as JLabel
				val flag = lbl.getClientProperty("flag") as Int
				val flagSet = f.get(flag)
				f.set(flag, !flagSet)
				updateFlagsAction.actionPerformed(ActionEvent(this, -1, "manual"))
			}
		}
		for (j in 7 downTo 0) {
			val lbl = JLabel(f.getName(j))
			lbl.putClientProperty("flag", j)
			lbl.addMouseListener(lblMouseListener)
			result.add(lbl)
		}
		result.add(JLabel())   // Flags as byte
		updateFlagsAction.actionPerformed(null)
		return result
	}

	private fun createRegLabel(reg: Register): JPanel {
		val lbl = reg.name
		val result = createCpuLabel(lbl, reg.size * 2, { reg.get() }, { arg1 ->
			if (arg1 < 0 || arg1 > 0xFFFF) {
				false
			} else {
				reg.set(arg1)
				true
			}
		})
		val lblObj = result.getComponent(0) as JLabel
		val updateLabelAction = object : AbstractAction() {
			private val _lblObj = lblObj
			private val _lbl = lbl
			private val _reg = reg
			private var oldValue: Short = 0

			override fun actionPerformed(e: ActionEvent?) {
				val value = _reg.get().toShort()
				if (_lblObj.text.isEmpty() || oldValue != value) {
					_lblObj.text = _lbl + ": " + String.format("%1$04X", value)
				}
				_lblObj.foreground = if (oldValue != value) COLOR_FORE_REGCHANGED else foreground
				oldValue = value
			}
		}
		onStepActions.put(lbl, updateLabelAction)
		updateLabelAction.actionPerformed(null)
		return result
	}

	private fun createIntRegLabel(regIdx: Int): JPanel {
		val counter = cpu.getCounter(regIdx)
		val lbl = counter.name
		val regSize = counter.size
		val result = createCpuLabel(lbl, regSize, { counter.get() }, { arg1 ->
			if (arg1 < 0 || arg1 > 0xFFFFFFFF.toInt().ushr(8 - regSize shl 2)) {
				false
			} else {
				counter.set(arg1)
				true
			}
		})
		val lblObj = result.getComponent(0) as JLabel
		val updateLabelAction = object : AbstractAction() {
			private val _lblObj = lblObj
			private val _lbl = lbl
			private val _regIdx = regIdx
			private val _regSize = regSize
			private var oldValue: Short = 0

			override fun actionPerformed(e: ActionEvent?) {
				val value = cpu.getCounter(_regIdx).get().toShort()
				if (_lblObj.text.isEmpty() || oldValue != value) {
					_lblObj.text = _lbl + ": " + String.format("%1$0" + _regSize + "X", value)
				}
				_lblObj.foreground = if (oldValue != value) COLOR_FORE_REGCHANGED else foreground
				oldValue = value
			}
		}
		onStepActions.put(lbl, updateLabelAction)
		updateLabelAction.actionPerformed(null)
		return result
	}

	private fun createCpuLabel(lbl: String, regSize: Int, defaultValueFunc: () -> Int, setValueFunc: (Int) -> Boolean): JPanel {
		val result = JPanel()
		result.background = background
		val lblObj = JLabel()
		lblObj.foreground = foreground
		result.border = BorderFactory.createBevelBorder(BevelBorder.LOWERED)
		result.add(lblObj)

		result.addMouseListener(object : MouseAdapter() {
			override fun mouseClicked(e: MouseEvent?) {
				val defaultValue = defaultValueFunc()
				val defaultValueStr = String.format("%1$0" + regSize + "X", defaultValue)
				val inputResult = JOptionPane.showInputDialog(this@Debugger, "Enter new value:", lbl + " Register", JOptionPane.QUESTION_MESSAGE, null, null, defaultValueStr) as String?
				if (inputResult != null) {
					try {
						val intObj = Integer.decode("0x" + inputResult)
						val setValueResult = setValueFunc(intObj)
						if (!setValueResult) {
							JOptionPane.showMessageDialog(this@Debugger, "Invalid value (check bounds or value itself)", "Invalid value", JOptionPane.ERROR_MESSAGE)
						} else {
							onStepActions[lbl]!!.actionPerformed(null)
						}
					} catch (ex: NumberFormatException) {
						JOptionPane.showMessageDialog(this@Debugger, "New value must be a positive hexadecimal number", "Invalid value", JOptionPane.ERROR_MESSAGE)
					}

				}
			}
		})
		return result
	}

	private fun onStep() {
		val pc = cpu.pc
		if (!asmTableModel.isRowDecoded(asmTableModel.addr2index(pc))) {
			asmTableModel.decodeAddress(pc)
		}
		for (dialog in debugDialogs) {
			dialog.onStep()
		}
	}

	private fun onStepGraphical() {
		onStep()
		for (dialog in debugDialogs) {
			dialog.onGraphicalStep()
		}
		for (act in onStepActions.values) {
			act.actionPerformed(null)
		}
	}

	fun gotoLine(idx: Int) {
		tblDisasm.setRowSelectionInterval(idx, idx)
		val cellRect = Rectangle(tblDisasm.getCellRect(idx, 0, true))
		val visRect = tblDisasm.visibleRect
		cellRect.size = visRect.size
		cellRect.translate(0, -visRect.height / 2)
		tblDisasm.scrollRectToVisible(cellRect)
	}

	inline fun getDecodedLine(addr: Register) = getDecodedLine(addr.get())
	fun getDecodedLine(addr: Int) = asmTableModel.getASMLine(asmTableModel.addr2index(addr))


	private inner class SimpleRenderer : DefaultTableCellRenderer() {
		override fun getTableCellRendererComponent(table: JTable?, value: Any?, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int): Component {
			val result = super.getTableCellRendererComponent(table, value, isSelected, false, row, column)
			val pc = cpu.pc
			if (asmTableModel.addr2index(pc) == row) {
				super.setBackground(COLOR_BACK_CURRENT)
			} else if (breakpointListModel.contains(ASMTableModel.BreakpointPosition(row))) {
				super.setBackground(COLOR_BACK_BREAKPOINT)
			} else {
				super.setBackground(if (isSelected) table!!.selectionBackground else table!!.background)
			}
			return result
		}
	}

	private inner class BreakpointRenderer : TableCellRenderer {
		private val painter = JLabel()
		private val ICON_BREAKPOINT: Icon

		init {
			painter.isOpaque = true
			painter.background = COLOR_BACK_GUTTER
			ICON_BREAKPOINT = ImageIcon(this.javaClass.getResource("/breakpoint.png"))
		}

		override fun getTableCellRendererComponent(table: JTable, value: Any?, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int): Component {
			painter.icon = if (value as Boolean? ?: false) ICON_BREAKPOINT else null
			painter.background = if (isSelected) table.selectionBackground else COLOR_BACK_GUTTER
			return painter
		}
	}
}
