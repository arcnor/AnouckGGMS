package com.arcnor.anouckmse.debugger.command

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog
import sun.jvm.hotspot.oops.CellTypeState.value
import java.awt.event.ActionEvent
import javax.swing.AbstractAction

class LogCommand(console: ConsoleDialog) : Command(console, "log", "Logs status of the CPU after every step", object : AbstractAction("Log Command") {
	override fun actionPerformed(e: ActionEvent) {
		LogCommand.logEnabled = !LogCommand.logEnabled
		console.append("Logging is now " + (if (LogCommand.logEnabled) "enabled" else "disabled") + '\n')
	}
}) {
	companion object {
		private var logEnabled = false
	}

	private val sb = StringBuilder()

	override val isStepCommand: Boolean
		get() = true

	override fun onStep() {
		if (logEnabled) {
			val cpu = console.debugger.cpu

			sb.setLength(0)

			val line = console.debugger.getDecodedLine(cpu.pc)

			for (b in line.buffer) {
				sb.append(String.format("%1$02X ", b))
			}
			sb.setLength(sb.length - 1)  // Trim last space
			sb.append('\t')
			// FIXME: ".decoded" is not right because it has the comment attached. Split that
			//			sb.append('\t').append(line.decoded);

			val nRegs = cpu.numRegisters
			for (j in 0..nRegs - 1) {
				val reg = cpu.getRegister(j)
				sb.append(reg.name).append(':').append(String.format("%1$04X", reg.get())).append(' ')
			}

			val nCounters = cpu.numCounters
			(0..nCounters - 1)
					.map { cpu.getCounter(it) }
					.forEach { sb.append(it.name).append(':').append(String.format("%1$0" + it.size + "X", it.get())).append(' ') }

			sb.append("F:").append(String.format("%1$02X", cpu.flags.get())).append(' ')
			sb.append("Cycle:").append(cpu.clockCycles)

			console.append(String.format("%1$04X\t%2\$s\n", cpu.pc, sb.toString()), ConsoleDialog.STYLE_DATA)
		}
	}
}
