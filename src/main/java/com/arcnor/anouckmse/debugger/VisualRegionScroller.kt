package com.arcnor.anouckmse.debugger

import java.awt.Adjustable
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Component
import java.awt.Dimension
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.event.ActionEvent
import java.awt.event.AdjustmentEvent
import java.awt.event.AdjustmentListener
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.util.HashMap
import javax.swing.AbstractAction
import javax.swing.Action
import javax.swing.BorderFactory
import javax.swing.JButton
import javax.swing.JPanel
import javax.swing.JScrollBar
import javax.swing.JScrollPane
import javax.swing.JViewport
import javax.swing.ScrollPaneConstants

internal class VisualRegionScroller(private val orientation: Int, private val asmModel: ASMTableModel<*>, asmDisplayVertScroll: JScrollBar) : JPanel(BorderLayout()) {
	private companion object {
		val COLOR_POS = Color(0x00, 0xFF, 0x00, 0x80)
	}

	private val colorCache = HashMap<Int, Color>()
	private var pos: Int = 0
	private var posSize: Int = 0
	private val scrollArea: VisualScrollArea

	init {
		preferredSize = Dimension(
				if (orientation == JScrollBar.HORIZONTAL) -1 else 50,
				if (orientation == JScrollBar.HORIZONTAL) 50 else -1
		)

		colorCache.put(0, Color.BLACK)

		scrollArea = VisualScrollArea(asmDisplayVertScroll)

		val scrollPane = JScrollPane(scrollArea)
		scrollPane.verticalScrollBarPolicy = ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER
		add(scrollPane, BorderLayout.CENTER)

		scrollArea.addMouseListener(object : MouseAdapter() {
			override fun mouseClicked(e: MouseEvent) {
				val panel = e.source as JPanel
				val relPos = e.x.toDouble() / panel.width
				asmDisplayVertScroll.value = (asmDisplayVertScroll.maximum * relPos).toInt() + 1
			}
		})

		val pnlInput = JPanel()
		pnlInput.layout = BorderLayout()
		val zoomIn = object : AbstractAction("+") {
			override fun actionPerformed(e: ActionEvent) {
				scrollArea.incZoom()
			}
		}
		val zoomOut = object : AbstractAction("-") {
			override fun actionPerformed(e: ActionEvent) {
				scrollArea.decZoom()
			}
		}
		val zoomAuto = object : AbstractAction("1:1") {
			override fun actionPerformed(e: ActionEvent) {
				scrollArea.resetZoom()
			}
		}
		pnlInput.add(createSmallButton(zoomIn), if (orientation == JScrollBar.HORIZONTAL) BorderLayout.NORTH else BorderLayout.WEST)
		pnlInput.add(createSmallButton(zoomAuto), BorderLayout.CENTER)
		pnlInput.add(createSmallButton(zoomOut), if (orientation == JScrollBar.HORIZONTAL) BorderLayout.SOUTH else BorderLayout.EAST)
		add(pnlInput, if (orientation == JScrollBar.HORIZONTAL) BorderLayout.EAST else BorderLayout.NORTH)
	}

	private fun createSmallButton(act: Action): Component {
		val result = JButton(act)
		result.border = BorderFactory.createEmptyBorder(0, 4, 0, 4)
		return result
	}

	private inner class VisualScrollArea(private val asmDisplayVertScroll: JScrollBar) : JPanel(), AdjustmentListener {
		private val asmModel = this@VisualRegionScroller.asmModel

		private val orientation = this@VisualRegionScroller.orientation
		private var zoom = 1
		private var normalWidth = -1
		private var scrollPane: JViewport? = null

		init {
			asmDisplayVertScroll.addAdjustmentListener(this)
		}

		override fun paintComponent(g: Graphics) {
			val g2 = g.create() as Graphics2D

			val rowNum = asmModel.rowCount
			val bounds = bounds
			val clip = g2.clipBounds
			if (orientation == JScrollBar.HORIZONTAL) {
				val ratio = rowNum.toFloat() / bounds.width
				val colorRatio = (ratio / 255f).toDouble()
				for (j in clip.x..clip.x + clip.width - 1) {
					val rowIndex = (j * ratio).toInt()
					var lineColor = 0
					var k = rowIndex
					while (k < rowIndex + ratio) {
						if (asmModel.isRowDecoded(k)) {
							lineColor++
						}
						k++
					}
					var c: Color? = colorCache[lineColor]
					if (c == null) {
						val comp = Math.min(0xFF, (lineColor / colorRatio).toInt())
						c = Color(comp, 0x80, 0xFF - comp)
						colorCache.put(lineColor, c)
					}
					// FIXME: Use fillRect instead, and cache last color so we do fills per block, not per line
					g2.color = c
					g2.drawLine(j, 0, j, clip.height - 1)

					if (posSize > 0) {
						//						float alpha = 0.10f;
						//						int type = AlphaComposite.SRC_OVER;
						//						AlphaComposite composite = AlphaComposite.getInstance(type, alpha);
						//						Composite origComposite = g2.getComposite();

						g2.color = COLOR_POS
						//						g2.setComposite(composite);
						g2.fillRect(pos, 0, posSize, clip.height)
						//						g2.setComposite(origComposite);
					}
				}
			}

			g2.dispose()
		}

		override fun addNotify() {
			super.addNotify()
			val parent = parent
			if (parent is JViewport) {
				this.scrollPane = parent
			}
		}

		override fun adjustmentValueChanged(e: AdjustmentEvent) {
			val adjustable = e.adjustable
			updatePosAndSize(adjustable)

			// FIXME: We should do dirty rects
			repaint()
		}

		private fun updatePosAndSize(adjustable: Adjustable) {
			val absPos = adjustable.value.toDouble() / adjustable.maximum
			val absPosSize = adjustable.visibleAmount.toDouble() / adjustable.maximum
			pos = (absPos * width).toInt()
			posSize = Math.max(1.0, absPosSize * width).toInt()
		}

		fun incZoom() {
			zoom++
			if (normalWidth <= 0) {
				normalWidth = width
			}
			// FIXME: View should be centered around previous point after doLayout()
			preferredSize = Dimension(normalWidth * zoom, height)
			scrollPane!!.doLayout()
			updatePosAndSize(asmDisplayVertScroll)
		}

		fun decZoom() {
			if (zoom > 1) {
				--zoom
				if (normalWidth <= 0) {
					normalWidth = width
				}
				preferredSize = Dimension(normalWidth * zoom, height)
				scrollPane!!.doLayout()
				updatePosAndSize(asmDisplayVertScroll)
			}
		}

		fun resetZoom() {
			zoom = 1
			preferredSize = null
			scrollPane!!.doLayout()
			normalWidth = width
		}
	}
}
