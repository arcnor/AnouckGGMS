package com.arcnor.anouckmse.debugger.command

import com.arcnor.anouckmse.debugger.Debugger
import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog

class ContinueCommand(console: ConsoleDialog) : StepCommand(console, "c", "Continue execution", Debugger.ACT_CONTINUE)
