package com.arcnor.vdp

import com.arcnor.system.VideoChip
import java.awt.Color

class CHIP8Graphics : VideoChip(64, 32) {
	companion object {
		private val COLOR_OFF = Color.BLACK
		private val COLOR_ON = Color.WHITE
	}

	private val mem = BooleanArray(width * height)

	init {
		clear()
	}

	fun xor(address: Int, pixel: Int): Boolean {
		val newValue = mem[address] xor (pixel > 0)
		mem[address] = newValue

		paint(address, newValue)

		return mem[address]
	}

	private fun paint(address: Int, value: Boolean) {
		val color = if (value) COLOR_ON else COLOR_OFF
		videoSurface.setRGB(address % width, address / width, color.rgb)
	}

	fun clear() {
		mem.fill(false)

		g2.color = COLOR_OFF
		g2.fillRect(0, 0, videoSurface.width, videoSurface.height)
	}
}