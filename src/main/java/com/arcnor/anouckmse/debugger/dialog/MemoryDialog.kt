package com.arcnor.anouckmse.debugger.dialog

import com.arcnor.anouckmse.debugger.Debugger
import com.arcnor.anouckmse.swing.Theming
import com.arcnor.system.MemoryChip
import tv.porst.jhexview.AbstractDataProvider
import tv.porst.jhexview.JHexView
import java.awt.BorderLayout
import java.awt.FlowLayout
import java.awt.event.ActionEvent
import javax.swing.AbstractAction
import javax.swing.JButton
import javax.swing.JPanel

class MemoryDialog(owner: Debugger, mem: MemoryChip) : DebugDialog(owner, "Memory", false) {
	private val memoryDataModel: MemoryDataModel

	init {
		layout = BorderLayout()

		val hexView = JHexView()
		hexView.backgroundColorAsciiView = Theming.COLOR_BACK_WINDOW
		hexView.backgroundColorHexView = Theming.COLOR_BACK_WINDOW
		hexView.fontColorAsciiView = Theming.COLOR_STRING
		hexView.fontColorHexView1 = Theming.COLOR_RESERVED
		hexView.fontColorHexView2 = Theming.COLOR_RESERVED.brighter()
		hexView.lineColorDivider = Theming.COLOR_LINE
		hexView.setSelectionColor(Theming.COLOR_SELECTION)
		hexView.backgroundColorOffsetView = Theming.COLOR_BACK_MENU
		hexView.fontColorOffsetView = Theming.COLOR_FORE

		memoryDataModel = MemoryDataModel(mem)
		hexView.data = memoryDataModel
		hexView.definitionStatus = JHexView.DefinitionStatus.DEFINED
		hexView.isEnabled = true
		add(hexView, BorderLayout.CENTER)
		val pnlButtons = JPanel()
		pnlButtons.layout = FlowLayout()
		add(pnlButtons, BorderLayout.SOUTH)

		val regionNames = mem.regionNames
		for (j in regionNames.indices) {
			val region = regionNames[j]
			val regionIdx = j
			pnlButtons.add(JButton(object : AbstractAction(region) {
				override fun actionPerformed(e: ActionEvent) {
					memoryDataModel.region = regionIdx
					hexView.baseAddress = memoryDataModel.regionOffset
				}
			}))
		}

		setSize(550, owner.height / 2)
		val ownerLoc = owner.location
		setLocation((ownerLoc.getX() - bounds.getWidth()).toInt(), ownerLoc.getY().toInt() + height)
	}

	override fun onStep() {
		// Do nothing, because we don't care that the memory dialog is out-of-sync during long runs
	}

	override fun onGraphicalStep() {
		memoryDataModel.onStep()
	}

	private inner class MemoryDataModel(private val mem: MemoryChip) : AbstractDataProvider() {
		var region: Int = 0
			set(idx) {
				field = idx

				notifyListeners()
			}

		fun onStep() {
			notifyListeners()
		}

		val regionOffset: Long
			get() = mem.getRegionOffset(this.region, 0)

		override fun getData(offset: Long, length: Long) = mem.getData(offset.toInt(), length.toInt())
		override fun getDataLength() = mem.getRegionLength(this.region)

		override fun setData(addr: Long, bytes: ByteArray) {
			for (j in bytes.indices) {
				mem.write((addr + j).toInt(), bytes[j])
			}
			notifyListeners()
		}

		override fun hasData(offset: Long, length: Long) = true
		override fun isEditable() = true
		override fun keepTrying() = false
	}
}
