package com.arcnor.anouckmse.debugger.command

import com.arcnor.anouckmse.debugger.Debugger
import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog

class StepOutCommand(console: ConsoleDialog) : StepCommand(console, "so", "Step out", Debugger.ACT_STEP_OUT)
