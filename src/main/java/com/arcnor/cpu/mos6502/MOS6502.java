package com.arcnor.cpu.mos6502;

import com.arcnor.cpu.Flags;
import com.arcnor.cpu.Register;
import com.arcnor.cpu.Register16;
import com.arcnor.cpu.Register8;
import com.arcnor.system.CPUChip;
import com.arcnor.system.MemoryChip;

public class MOS6502 implements CPUChip {
	private int clock, counter;

	static final int OP_ADC_IX = 0x61;
	static final int OP_ADC_0  = 0x65;
	static final int OP_ADC_IM = 0x69;
	static final int OP_ADC_AB = 0x6D;
	static final int OP_ADC_IY = 0x71;
	static final int OP_ADC_0X = 0x75;
	static final int OP_ADC_AY = 0x79;
	static final int OP_ADC_AX = 0x7D;

	static final int OP_AND_IX = 0x21;
	static final int OP_AND_0  = 0x25;
	static final int OP_AND_IM = 0x29;
	static final int OP_AND_AB = 0x2D;
	static final int OP_AND_IY = 0x31;
	static final int OP_AND_0X = 0x35;
	static final int OP_AND_AY = 0x39;
	static final int OP_AND_AX = 0x3D;

	static final int OP_ASL_0  = 0x06;
	static final int OP_ASL    = 0x0A;
	static final int OP_ASL_AB = 0x0E;
	static final int OP_ASL_0X = 0x16;
	static final int OP_ASL_AX = 0x1E;

	static final int OP_BCC = 0x90;
	static final int OP_BCS = 0xB0;
	static final int OP_BEQ = 0xF0;

	static final int OP_BIT_0 = 0x24;
	static final int OP_BIT_AB = 0x2C;

	static final int OP_BMI = 0x30;
	static final int OP_BNE = 0xD0;
	static final int OP_BPL = 0x10;
	static final int OP_BRK = 0x00;
	static final int OP_BVC = 0x50;
	static final int OP_BVS = 0x70;
	static final int OP_CLC = 0x18;
	static final int OP_CLD = 0xD8;
	static final int OP_CLI = 0x58;
	static final int OP_CLV = 0xB8;

	static final int OP_CMP_IX = 0xC1;
	static final int OP_CMP_0  = 0xC5;
	static final int OP_CMP_IM = 0xC9;
	static final int OP_CMP_AB = 0xCD;
	static final int OP_CMP_IY = 0xD1;
	static final int OP_CMP_0X = 0xD5;
	static final int OP_CMP_AY = 0xD9;
	static final int OP_CMP_AX = 0xDD;

	static final int OP_CPX_IM = 0xE0;
	static final int OP_CPX_0  = 0xE4;
	static final int OP_CPX_AB = 0xEC;

	static final int OP_CPY_IM = 0xC0;
	static final int OP_CPY_0  = 0xC4;
	static final int OP_CPY_AB = 0xCC;

	static final int OP_DEC_0  = 0xC6;
	static final int OP_DEC_AB = 0xCE;
	static final int OP_DEC_0X = 0xD6;
	static final int OP_DEC_AX = 0xDE;

	static final int OP_DEX = 0xCA;
	static final int OP_DEY = 0x88;

	static final int OP_EOR_IX = 0x41;
	static final int OP_EOR_0  = 0x45;
	static final int OP_EOR_IM = 0x49;
	static final int OP_EOR_AB = 0x4D;
	static final int OP_EOR_IY = 0x51;
	static final int OP_EOR_0X = 0x55;
	static final int OP_EOR_AY = 0x59;
	static final int OP_EOR_AX = 0x5D;

	static final int OP_INC_0  = 0xE6;
	static final int OP_INC_AB = 0xEE;
	static final int OP_INC_0X = 0xF6;
	static final int OP_INC_AX = 0xFE;

	static final int OP_INX = 0xE8;
	static final int OP_INY = 0xC8;

	static final int OP_JMP_AB = 0x4C;
	static final int OP_JMP_IN = 0x6C;

	static final int OP_JSR = 0x20;

	static final int OP_LDA_IX = 0xA1;
	static final int OP_LDA_0  = 0xA5;
	static final int OP_LDA_IM = 0xA9;
	static final int OP_LDA_AB = 0xAD;
	static final int OP_LDA_IY = 0xB1;
	static final int OP_LDA_0X = 0xB5;
	static final int OP_LDA_AY = 0xB9;
	static final int OP_LDA_AX = 0xBD;

	static final int OP_LDX_IM = 0xA2;
	static final int OP_LDX_0  = 0xA6;
	static final int OP_LDX_AB = 0xAE;
	static final int OP_LDX_0Y = 0xB6;
	static final int OP_LDX_AY = 0xBE;

	static final int OP_LDY_IM = 0xA0;
	static final int OP_LDY_0  = 0xA4;
	static final int OP_LDY_AB = 0xAC;
	static final int OP_LDY_0X = 0xB4;
	static final int OP_LDY_AX = 0xBC;

	static final int OP_LSR_0  = 0x46;
	static final int OP_LSR    = 0x4A;
	static final int OP_LSR_AB = 0x4E;
	static final int OP_LSR_0X = 0x56;
	static final int OP_LSR_AX = 0x5E;

	static final int OP_NOP = 0xEA;

	static final int OP_ORA_IX = 0x01;
	static final int OP_ORA_0  = 0x05;
	static final int OP_ORA_IM = 0x09;
	static final int OP_ORA_AB = 0x0D;
	static final int OP_ORA_IY = 0x11;
	static final int OP_ORA_0X = 0x15;
	static final int OP_ORA_AY = 0x19;
	static final int OP_ORA_AX = 0x1D;

	static final int OP_PHA = 0x48;
	static final int OP_PHP = 0x08;
	static final int OP_PLA = 0x68;
	static final int OP_PLP = 0x28;

	static final int OP_ROL_0  = 0x26;
	static final int OP_ROL    = 0x2A;
	static final int OP_ROL_AB = 0x2E;
	static final int OP_ROL_0X = 0x36;
	static final int OP_ROL_AX = 0x3E;

	static final int OP_ROR_0  = 0x66;
	static final int OP_ROR    = 0x6A;
	static final int OP_ROR_AB = 0x6E;
	static final int OP_ROR_0X = 0x76;
	static final int OP_ROR_AX = 0x7E;

	static final int OP_RTI = 0x40;
	static final int OP_RTS = 0x60;

	static final int OP_SBC_IX = 0xE1;
	static final int OP_SBC_0  = 0xE5;
	static final int OP_SBC_IM = 0xE9;
	static final int OP_SBC_AB = 0xED;
	static final int OP_SBC_IY = 0xF1;
	static final int OP_SBC_0X = 0xF5;
	static final int OP_SBC_AY = 0xF9;
	static final int OP_SBC_AX = 0xFD;

	static final int OP_SEC = 0x38;
	static final int OP_SED = 0xF8;
	static final int OP_SEI = 0x78;

	static final int OP_STA_IX = 0x81;
	static final int OP_STA_0  = 0x85;
	static final int OP_STA_AB = 0x8D;
	static final int OP_STA_IY = 0x91;
	static final int OP_STA_0X = 0x95;
	static final int OP_STA_AY = 0x99;
	static final int OP_STA_AX = 0x9D;

	static final int OP_STX_0  = 0x86;
	static final int OP_STX_AB = 0x8E;
	static final int OP_STX_0Y = 0x96;

	static final int OP_STY_0  = 0x84;
	static final int OP_STY_AB = 0x8C;
	static final int OP_STY_0X = 0x94;

	static final int OP_TAX = 0xAA;
	static final int OP_TAY = 0xA8;
	static final int OP_TSX = 0xBA;
	static final int OP_TXA = 0x8A;
	static final int OP_TXS = 0x9A;
	static final int OP_TYA = 0x98;

	private final MemoryChip mem;

	private Register PC = new Register16("PC"), SP = new Register8("SP");
	private Register A = new Register8("A"), X = new Register8("X"), Y = new Register8("Y");
	private Flags F = new MOS6502Flags();

	public MOS6502(final MemoryChip mem) {
		this.mem = mem;

		reset();
	}

	@Override
	public void reset() {
		PC.set((int)getPorAddr());
		SP.set(0xFD);
		A.set(0xAA);
		X.set(0);
		Y.set(0);
		F.reset();
	}

	private int incPC() {
		int oldPC = PC.get();
		PC.inc();
		return oldPC;
	}

	private void addPC(int offset) {
		int oldPage = PC.get() & 0xFF00;
		PC.set((PC.get() + offset) & 0xFFFF);
		clock += oldPage != (PC.get() & 0xFF00) ? 2 : 1;
	}

	private void setPC(int addr) {
		PC.set(addr & 0xFFFF);
	}

	private int decSP() {
		int oldSP = SP.get();
		SP.set((SP.get() - 1) & 0xFF);
		return oldSP;
	}

	private int incSP() {
		SP.set((SP.get() + 1) & 0xFF);
		return SP.get();
	}

	private void setA(int value) {
		A.set(value);
		F.set(MOS6502Flags.FLAG_ZERO, value == 0);
		F.set(MOS6502Flags.FLAG_NEGATIVE, (value & 0x80) != 0);
	}

	private void setX(int value) {
		X.set(value);
		F.set(MOS6502Flags.FLAG_ZERO, value == 0);
		F.set(MOS6502Flags.FLAG_NEGATIVE, (value & 0x80) != 0);
	}

	private void setY(int value) {
		Y.set(value);
		F.set(MOS6502Flags.FLAG_ZERO, value == 0);
		F.set(MOS6502Flags.FLAG_NEGATIVE, (value & 0x80) != 0);
	}

	private void setSP(int value) {
		SP.set(value);
	}

	private void incMem(int addr) {
		int value = (readUnsignedMem8(addr) + 1) & 0xFF;
		F.set(MOS6502Flags.FLAG_ZERO, value == 0);
		F.set(MOS6502Flags.FLAG_NEGATIVE, (value & 0x80) != 0);
		writeMem8(addr, (byte)value);
	}

	private void decMem(int addr) {
		int value = (readUnsignedMem8(addr) - 1) & 0xFF;
		F.set(MOS6502Flags.FLAG_ZERO, value == 0);
		F.set(MOS6502Flags.FLAG_NEGATIVE, (value & 0x80) != 0);
		writeMem8(addr, (byte)value);
	}

	private void branchIfSet(int flag) {
		int data = readMem8();
		if (F.get(flag)) {
			addPC(data);
		}
	}

	private void branchIfClear(int flag) {
		int data = readMem8();
		if (!F.get(flag)) {
			addPC(data);
		}
	}

	private void compare(int value, Register reg) {
		F.set(MOS6502Flags.FLAG_CARRY, reg.get() >= value);
		value = (reg.get() - value) & 0xFF;
		F.set(MOS6502Flags.FLAG_ZERO, value == 0);
		F.set(MOS6502Flags.FLAG_NEGATIVE, (value & 0x80) != 0);
	}

	private void adc(int value) {
		int c = F.get(MOS6502Flags.FLAG_CARRY) ? 1 : 0;
		int temp = value + A.get() + c;
		F.set(MOS6502Flags.FLAG_ZERO, (temp & 0xFF) == 0);
		if (!F.get(MOS6502Flags.FLAG_DECIMAL)) {
			F.set(MOS6502Flags.FLAG_NEGATIVE, (temp & 0x80) != 0);
			F.set(MOS6502Flags.FLAG_OVERFLOW, (((A.get() ^ value) & 0x80) == 0) && (((A.get() ^ temp) & 0x80)) != 0);
			F.set(MOS6502Flags.FLAG_CARRY, (temp & 0x100) != 0);
		} else {
			// FIXME: BCD mode
			if (((A.get() & 0x0F) + (value & 0x0F) + c) > 9) {
				temp += 6;
			}
			F.set(MOS6502Flags.FLAG_NEGATIVE, (temp & 0x80) != 0);
			F.set(MOS6502Flags.FLAG_OVERFLOW, (((A.get() ^ value) & 0x80) == 0) && (((A.get() ^ temp) & 0x80)) != 0);
			if (temp > 0x99) {
				temp += 96;
			}
			F.set(MOS6502Flags.FLAG_CARRY, temp > 0x99);
		}
		A.set(temp & 0xFF);
	}

	private void sbc(int value) {
		int c = F.get(MOS6502Flags.FLAG_CARRY) ? 0 : 1;
		int temp = A.get() - value - c;
		F.set(MOS6502Flags.FLAG_NEGATIVE, (temp & 0x80) != 0);
		F.set(MOS6502Flags.FLAG_ZERO, (temp & 0xFF) == 0);
		F.set(MOS6502Flags.FLAG_OVERFLOW, (((A.get() ^ value) & 0x80) != 0) && (((A.get() ^ temp) & 0x80)) != 0);
		if (F.get(MOS6502Flags.FLAG_DECIMAL)) {
			if ( ((A.get() & 0x0F) - c) < (value & 0x0F)) {
				temp -= 6;
			}
			if (temp > 0x99) {
				temp -= 0x60;
			}
		}
		F.set(MOS6502Flags.FLAG_CARRY, (temp & 0xFFFF) < 0x100);
		A.set(temp & 0xFF);
	}

	private void aslA() {
		F.set(MOS6502Flags.FLAG_CARRY, (A.get() & 0x80) != 0);
		A.set(A.get() << 1);
		A.set(A.get() & 0xFF);
		F.set(MOS6502Flags.FLAG_NEGATIVE, (A.get() & 0x80) != 0);
		F.set(MOS6502Flags.FLAG_ZERO, A.get() == 0);
	}

	private void asl(int addr) {
		int value = readUnsignedMem8(addr);
		F.set(MOS6502Flags.FLAG_CARRY, (value & 0x80) != 0);
		value <<= 1;
		value &= 0xFF;
		F.set(MOS6502Flags.FLAG_NEGATIVE, (value & 0x80) != 0);
		F.set(MOS6502Flags.FLAG_ZERO, value == 0);
		writeMem8(addr, (byte) value);
	}

	private void lsrA() {
		F.set(MOS6502Flags.FLAG_CARRY, (A.get() & 0x01) != 0);
		A.set(A.get() >> 1);
		F.set(MOS6502Flags.FLAG_NEGATIVE, false);
		F.set(MOS6502Flags.FLAG_ZERO, A.get() == 0);
	}

	private void lsr(int addr) {
		int value = readUnsignedMem8(addr);
		F.set(MOS6502Flags.FLAG_CARRY, (value & 0x01) != 0);
		value >>= 1;
		F.set(MOS6502Flags.FLAG_NEGATIVE, false);
		F.set(MOS6502Flags.FLAG_ZERO, value == 0);
		writeMem8(addr, (byte) value);
	}

	private void rolA() {
		A.set(A.get() << 1);
		if (F.get(MOS6502Flags.FLAG_CARRY)) {
			A.set(A.get() | 0x01);
		}
		F.set(MOS6502Flags.FLAG_CARRY, (A.get() & 0x100) != 0);
		A.set(A.get() & 0xFF);
		F.set(MOS6502Flags.FLAG_NEGATIVE, (A.get() & 0x80) != 0);
		F.set(MOS6502Flags.FLAG_ZERO, A.get() == 0);
	}

	private void rol(int addr) {
		int value = readUnsignedMem8(addr);
		value <<= 1;
		if (F.get(MOS6502Flags.FLAG_CARRY)) {
			value |= 0x01;
		}
		F.set(MOS6502Flags.FLAG_CARRY, (value & 0x100) != 0);
		value &= 0xFF;
		F.set(MOS6502Flags.FLAG_NEGATIVE, (value & 0x80) != 0);
		F.set(MOS6502Flags.FLAG_ZERO, value == 0);
		writeMem8(addr, (byte)value);
	}

	private void rorA() {
		if (F.get(MOS6502Flags.FLAG_CARRY)) {
			A.set(A.get() | 0x100);
		}
		F.set(MOS6502Flags.FLAG_CARRY, (A.get() & 0x01) != 0);
		A.set(A.get() >> 1);
		A.set(A.get() & 0xFF);
		F.set(MOS6502Flags.FLAG_NEGATIVE, (A.get() & 0x80) != 0);
		F.set(MOS6502Flags.FLAG_ZERO, A.get() == 0);
	}

	private void ror(int addr) {
		int value = readUnsignedMem8(addr);
		if (F.get(MOS6502Flags.FLAG_CARRY)) {
			value |= 0x100;
		}
		F.set(MOS6502Flags.FLAG_CARRY, (value & 0x01) != 0);
		value >>= 1;
		value &= 0xFF;
		F.set(MOS6502Flags.FLAG_NEGATIVE, (value & 0x80) != 0);
		F.set(MOS6502Flags.FLAG_ZERO, value == 0);
		writeMem8(addr, (byte)value);
	}

	private byte readMem8() {
		return mem.readSigned(incPC());
	}

	private void writeMem8(int addr, byte value) {
		clock++;
		mem.write(addr, value);
	}

	private int readUnsignedMem8() {
		return mem.readUnsigned(incPC());
	}

	/**
	 * Returns the address from (M[PC] + X) & 0xFF
	 */
	private int addrAt_0X() {
		clock++;
		return (mem.readUnsigned(incPC()) + X.get()) & 0xFF;
	}

	/**
	 * Returns the address from (M[PC] + Y) & 0xFF
	 */
	private int addrAt_0Y() {
		clock++;
		return (mem.readUnsigned(incPC()) + Y.get()) & 0xFF;
	}

	private void writeMem8_0X(byte value) {
		clock++;
		mem.write((mem.readUnsigned(incPC()) + X.get()) & 0xFF, value);
	}

	private void writeMem8_0Y(byte value) {
		clock++;
		mem.write((mem.readUnsigned(incPC()) + Y.get()) & 0xFF, value);
	}

	private int addrAt_IX() {
		clock += 2;
		int arg = mem.readUnsigned(incPC());
		return
				mem.readUnsigned((arg + X.get()) & 0xFF) |
				mem.readUnsigned((arg + (X.get() + 1)) & 0xFF) << 8;
	}

	private int addrAt_IY() {
		clock++;
		int arg = mem.readUnsigned(incPC());
		int addr = readUnsignedMem8(arg) | (readUnsignedMem8((arg + 1) & 0xFF) << 8);
		int oldPage = addr & 0xFF00;
		addr = (addr + Y.get()) & 0xFFFF;
		if (oldPage != (addr & 0xFF00)) {
			clock++;
		}
		return addr;
	}

	private void writeMem8_IX(byte value) {
		clock += 2;
		int arg = mem.readUnsigned(incPC());
		mem.write(
				mem.readUnsigned((arg + X.get()) & 0xFF) |
				mem.readUnsigned((arg + (X.get() + 1)) & 0xFF) << 8,
			value);
	}

	private void writeMem8_IY(byte value) {
		clock++;
		int arg = mem.readUnsigned(incPC());
		int addr = readUnsignedMem8(arg) | (readUnsignedMem8((arg + 1) & 0xFF) << 8);
		int oldPage = addr & 0xFF00;
		addr = (addr + Y.get()) & 0xFFFF;
		if (oldPage != (addr & 0xFF00)) {
			clock++;
		}
		mem.write(addr, value);
	}

	private int readUnsignedMem8(int addr) {
		clock++;
		return mem.readUnsigned(addr);
	}

	private int readUnsignedMem16() {
		clock++;
		return (mem.readUnsigned(incPC()) | mem.readUnsigned(incPC()) << 8);
	}

	private int addrAt_X() {
		clock += 2;
		int addr = (mem.readUnsigned(incPC()) | mem.readUnsigned(incPC()) << 8);
		int oldPage = addr & 0xFF00;
		addr = (addr + X.get()) & 0xFFFF;
		if ((addr & 0xFF00) != oldPage) {
			clock++;
		}
		return addr;
	}

	private int addrAt_Y() {
		clock += 2;
		int addr = (mem.readUnsigned(incPC()) | mem.readUnsigned(incPC()) << 8);
		int oldPage = addr & 0xFF00;
		addr = (addr + Y.get()) & 0xFFFF;
		if (oldPage != (addr & 0xFF00)) {
			clock++;
		}
		return addr;
	}

	private void writeMem8_X(byte value) {
		clock += 2;
		int addr = (mem.readUnsigned(incPC()) | mem.readUnsigned(incPC()) << 8);
		int oldPage = addr & 0xFF00;
		addr = (addr + X.get()) & 0xFFFF;
		if (oldPage != (addr & 0xFF00)) {
			clock++;
		}
		mem.write(addr, value);
	}

	private void writeMem8_Y(byte value) {
		clock += 2;
		int addr = (mem.readUnsigned(incPC()) | mem.readUnsigned(incPC()) << 8);
		int oldPage = addr & 0xFF00;
		addr = (addr + Y.get()) & 0xFFFF;
		if (oldPage != (addr & 0xFF00)) {
			clock++;
		}
		mem.write(addr, value);
	}

	private int readUnsignedMem16(final int addr) {
		clock += 2;
		return (mem.readUnsigned(addr) | mem.readUnsigned(addr + 1) << 8);
	}

	private int fetch() {
		int op = mem.readUnsigned(incPC());

		// Add cycles
		counter -= 2;
		clock += 2;

		return op;
	}

	@Override
	public void doCycle() {
		int op = fetch();

		decodeOp(op);
	}

	private void decodeOp(int op) {
		switch (op) {
			case OP_ADC_IX: adc(readUnsignedMem8(addrAt_IX())); break;
			case OP_ADC_0:  adc(readUnsignedMem8(readUnsignedMem8())); break;
			case OP_ADC_IM: adc(readUnsignedMem8()); break;
			case OP_ADC_AB: adc(readUnsignedMem8(readUnsignedMem16())); break;
			case OP_ADC_IY: adc(readUnsignedMem8(addrAt_IY())); break;
			case OP_ADC_0X: adc(readUnsignedMem8(addrAt_0X())); break;
			case OP_ADC_AY: adc(readUnsignedMem8(addrAt_Y())); break;
			case OP_ADC_AX: adc(readUnsignedMem8(addrAt_X())); break;

			case OP_AND_IX: setA(A.get() & readUnsignedMem8(addrAt_IX())); break;
			case OP_AND_0:  setA(A.get() & readUnsignedMem8(readUnsignedMem8())); break;
			case OP_AND_IM: setA(A.get() & readUnsignedMem8()); break;
			case OP_AND_AB: setA(A.get() & readUnsignedMem8(readUnsignedMem16())); break;
			case OP_AND_IY: setA(A.get() & readUnsignedMem8(addrAt_IY())); break;
			case OP_AND_0X: setA(A.get() & readUnsignedMem8(addrAt_0X())); break;
			case OP_AND_AY: setA(A.get() & readUnsignedMem8(addrAt_Y())); break;
			case OP_AND_AX: setA(A.get() & readUnsignedMem8(addrAt_X())); break;

			case OP_ASL_0:  asl(readUnsignedMem8()); break;
			case OP_ASL:    aslA(); break;
			case OP_ASL_AB: asl(readUnsignedMem16()); break;
			case OP_ASL_0X: asl(addrAt_0X()); break;
			case OP_ASL_AX: asl(addrAt_X()); break;   // FIXME

			case OP_BIT_0: {
				int data = readUnsignedMem8(readUnsignedMem8());
				int bit = A.get() & data;
				F.set(MOS6502Flags.FLAG_ZERO, bit == 0);
				F.set(MOS6502Flags.FLAG_NEGATIVE, (data & 0x80) != 0);
				F.set(MOS6502Flags.FLAG_OVERFLOW, (data & 0x40) != 0);
				break;
			}
			case OP_BIT_AB: {
				int data = readUnsignedMem8(readUnsignedMem16());
				int bit = A.get() & data;
				F.set(MOS6502Flags.FLAG_ZERO, bit == 0);
				F.set(MOS6502Flags.FLAG_NEGATIVE, (data & 0x80) != 0);
				F.set(MOS6502Flags.FLAG_OVERFLOW, (data & 0x40) != 0);
				break;
			}

			case OP_BRK:
				break;

			// Branching //
			case OP_BCC: branchIfClear(MOS6502Flags.FLAG_CARRY); break;
			case OP_BCS: branchIfSet(MOS6502Flags.FLAG_CARRY); break;
			case OP_BNE: branchIfClear(MOS6502Flags.FLAG_ZERO); break;
			case OP_BEQ: branchIfSet(MOS6502Flags.FLAG_ZERO); break;
			case OP_BPL: branchIfClear(MOS6502Flags.FLAG_NEGATIVE); break;
			case OP_BMI: branchIfSet(MOS6502Flags.FLAG_NEGATIVE); break;
			case OP_BVC: branchIfClear(MOS6502Flags.FLAG_OVERFLOW); break;
			case OP_BVS: branchIfSet(MOS6502Flags.FLAG_OVERFLOW); break;

			case OP_CMP_IX: compare(readUnsignedMem8(addrAt_IX()), A); break;
			case OP_CMP_0:  compare(readUnsignedMem8(readUnsignedMem8()), A); break;
			case OP_CMP_IM: compare(readUnsignedMem8(), A); break;
			case OP_CMP_AB: compare(readUnsignedMem8(readUnsignedMem16()), A); break;
			case OP_CMP_IY: compare(readUnsignedMem8(addrAt_IY()), A); break;
			case OP_CMP_0X: compare(readUnsignedMem8(addrAt_0X()), A); break;
			case OP_CMP_AY: compare(readUnsignedMem8(addrAt_Y()), A); break;
			case OP_CMP_AX: compare(readUnsignedMem8(addrAt_X()), A); break;

			case OP_CPX_IM: compare(readUnsignedMem8(), X); break;
			case OP_CPX_0:  compare(readUnsignedMem8(readUnsignedMem8()), X); break;
			case OP_CPX_AB: compare(readUnsignedMem8(readUnsignedMem16()), X); break;

			case OP_CPY_IM: compare(readUnsignedMem8(), Y); break;
			case OP_CPY_0:  compare(readUnsignedMem8(readUnsignedMem8()), Y); break;
			case OP_CPY_AB: compare(readUnsignedMem8(readUnsignedMem16()), Y); break;

			case OP_DEC_0:  decMem(readUnsignedMem8()); break;
			case OP_DEC_AB: decMem(readUnsignedMem16()); break;
			case OP_DEC_0X: decMem(addrAt_0X()); break;
			case OP_DEC_AX: decMem(addrAt_X()); break;

			case OP_EOR_IX: setA(A.get() ^ readUnsignedMem8(addrAt_IX())); break;
			case OP_EOR_0:  setA(A.get() ^ readUnsignedMem8(readUnsignedMem8())); break;
			case OP_EOR_IM: setA(A.get() ^ readUnsignedMem8()); break;
			case OP_EOR_AB: setA(A.get() ^ readUnsignedMem8(readUnsignedMem16())); break;
			case OP_EOR_IY: setA(A.get() ^ readUnsignedMem8(addrAt_IY())); break;
			case OP_EOR_0X: setA(A.get() ^ readUnsignedMem8(addrAt_0X())); break;
			case OP_EOR_AY: setA(A.get() ^ readUnsignedMem8(addrAt_Y())); break;
			case OP_EOR_AX: setA(A.get() ^ readUnsignedMem8(addrAt_X())); break;

			case OP_INC_0:  incMem(readUnsignedMem8()); break;
			case OP_INC_AB: incMem(readUnsignedMem16()); break;
			case OP_INC_0X: incMem(addrAt_0X()); break;
			case OP_INC_AX: incMem(addrAt_X()); break;

			case OP_INX: setX((X.get() + 1) & 0xFF); break;
			case OP_INY: setY((Y.get() + 1) & 0xFF); break;

			case OP_DEX: setX((X.get() - 1) & 0xFF); break;
			case OP_DEY: setY((Y.get() - 1) & 0xFF); break;

			case OP_JMP_AB: setPC(readUnsignedMem16()); break;
			case OP_JMP_IN: setPC(readUnsignedMem16(readUnsignedMem16())); break;

			case OP_JSR: {
				int addr = readUnsignedMem16();
				int newPC = PC.get() - 1;
				writeMem8(0x100 | decSP(), (byte) ((newPC & 0xFF00) >> 8));
				writeMem8(0x100 | decSP(), (byte) (newPC & 0x00FF));
				setPC(addr);
				break;
			}

			case OP_LDA_IX: setA(readUnsignedMem8(addrAt_IX())); break;
			case OP_LDA_0:  setA(readUnsignedMem8(readUnsignedMem8())); break;
			case OP_LDA_IM: setA(readUnsignedMem8()); break;
			case OP_LDA_AB: setA(readUnsignedMem8(readUnsignedMem16())); break;
			case OP_LDA_IY: setA(readUnsignedMem8(addrAt_IY())); break;
			case OP_LDA_0X: setA(readUnsignedMem8(addrAt_0X())); break;
			case OP_LDA_AY: setA(readUnsignedMem8(addrAt_Y())); break;
			case OP_LDA_AX: setA(readUnsignedMem8(addrAt_X())); break;

			case OP_LDX_IM: setX(readUnsignedMem8()); break;
			case OP_LDX_0:  setX(readUnsignedMem8(readUnsignedMem8())); break;
			case OP_LDX_AB: setX(readUnsignedMem8(readUnsignedMem16())); break;
			case OP_LDX_0Y: setX(readUnsignedMem8(addrAt_0Y())); break;
			case OP_LDX_AY: setX(readUnsignedMem8(addrAt_Y())); break;

			case OP_LDY_IM: setY(readUnsignedMem8()); break;
			case OP_LDY_0:  setY(readUnsignedMem8(readUnsignedMem8())); break;
			case OP_LDY_AB: setY(readUnsignedMem8(readUnsignedMem16())); break;
			case OP_LDY_0X: setY(readUnsignedMem8(addrAt_0X())); break;
			case OP_LDY_AX: setY(readUnsignedMem8(addrAt_X())); break;

			case OP_LSR_0:  lsr(readUnsignedMem8()); break;
			case OP_LSR:    lsrA(); break;
			case OP_LSR_AB: lsr(readUnsignedMem16()); break;
			case OP_LSR_0X: lsr(addrAt_0X()); break;
			case OP_LSR_AX: lsr(addrAt_X()); break;

			case OP_NOP:
				break;

			case OP_ORA_IX: setA(A.get() | readUnsignedMem8(addrAt_IX())); break;
			case OP_ORA_0:  setA(A.get() | readUnsignedMem8(readUnsignedMem8())); break;
			case OP_ORA_IM: setA(A.get() | readUnsignedMem8()); break;
			case OP_ORA_AB: setA(A.get() | readUnsignedMem8(readUnsignedMem16())); break;
			case OP_ORA_IY: setA(A.get() | readUnsignedMem8(addrAt_IY())); break;
			case OP_ORA_0X: setA(A.get() | readUnsignedMem8(addrAt_0X())); break;
			case OP_ORA_AY: setA(A.get() | readUnsignedMem8(addrAt_Y())); break;
			case OP_ORA_AX: setA(A.get() | readUnsignedMem8(addrAt_X())); break;

			case OP_PHA: writeMem8(0x100 | decSP(), (byte) A.get()); break;
			case OP_PHP: writeMem8(0x100 | decSP(), (byte) F.get()); break;
			case OP_PLA: setA(readUnsignedMem8(0x100 | incSP())); break;
			case OP_PLP: F.set(readUnsignedMem8(0x100 | incSP())); break;

			case OP_ROL_0:  rol(readUnsignedMem8()); break;
			case OP_ROL:    rolA(); break;
			case OP_ROL_AB: rol(readUnsignedMem16()); break;
			case OP_ROL_0X: rol(addrAt_0X()); break;
			case OP_ROL_AX: rol(addrAt_X()); break;

			case OP_ROR_0:  ror(readUnsignedMem8()); break;
			case OP_ROR:    rorA(); break;
			case OP_ROR_AB: ror(readUnsignedMem16()); break;
			case OP_ROR_0X: ror(addrAt_0X()); break;
			case OP_ROR_AX: ror(addrAt_X()); break;

			case OP_RTI: {
				int P = readUnsignedMem8(0x100 | incSP());
				F.set(P);
				int addr = readUnsignedMem8(0x100 | incSP());
				addr |= readUnsignedMem8(0x100 | incSP()) << 8;
				setPC(addr);
				break;
			}
			case OP_RTS: {
				int addr = readUnsignedMem8(0x100 | incSP());
				addr |= readUnsignedMem8(0x100 | incSP()) << 8;
				setPC(addr + 1);
				break;
			}

			case OP_SBC_IX: sbc(readUnsignedMem8(addrAt_IX())); break;
			case OP_SBC_0:  sbc(readUnsignedMem8(readUnsignedMem8())); break;
			case OP_SBC_IM: sbc(readUnsignedMem8()); break;
			case OP_SBC_AB: sbc(readUnsignedMem8(readUnsignedMem16())); break;
			case OP_SBC_IY: sbc(readUnsignedMem8(addrAt_IY())); break;
			case OP_SBC_0X: sbc(readUnsignedMem8(addrAt_0X())); break;
			case OP_SBC_AY: sbc(readUnsignedMem8(addrAt_Y())); break;
			case OP_SBC_AX: sbc(readUnsignedMem8(addrAt_X())); break;

			// Flag setting //
			case OP_SEC: F.set(MOS6502Flags.FLAG_CARRY, true); break;
			case OP_SED: F.set(MOS6502Flags.FLAG_DECIMAL, true); break;
			case OP_SEI: F.set(MOS6502Flags.FLAG_INTERRUPT, true); break;

			// Flag clearing //
			case OP_CLC: F.set(MOS6502Flags.FLAG_CARRY, false); break;
			case OP_CLD: F.set(MOS6502Flags.FLAG_DECIMAL, false); break;
			case OP_CLI: F.set(MOS6502Flags.FLAG_INTERRUPT, false); break;
			case OP_CLV: F.set(MOS6502Flags.FLAG_OVERFLOW, false); break;

			case OP_STA_IX: writeMem8_IX((byte) A.get()); break;
			case OP_STA_0:  writeMem8(readUnsignedMem8(), (byte) A.get()); break;
			case OP_STA_AB: writeMem8(readUnsignedMem16(), (byte) A.get()); break;
			case OP_STA_IY: writeMem8_IY((byte) A.get()); break;
			case OP_STA_0X: writeMem8_0X((byte) A.get()); break;
			case OP_STA_AY: writeMem8_Y((byte) A.get()); break;
			case OP_STA_AX: writeMem8_X((byte) A.get()); break;

			case OP_STX_0:  writeMem8(readUnsignedMem8(), (byte) X.get()); break;
			case OP_STX_AB: writeMem8(readUnsignedMem16(), (byte) X.get()); break;
			case OP_STX_0Y: writeMem8_0Y((byte) X.get()); break;

			case OP_STY_0:  writeMem8(readUnsignedMem8(), (byte) Y.get()); break;
			case OP_STY_AB: writeMem8(readUnsignedMem16(), (byte) Y.get()); break;
			case OP_STY_0X: writeMem8_0X((byte) Y.get()); break;

			case OP_TAX: setX(A.get()); break;
			case OP_TXA: setA(X.get()); break;
			case OP_TAY: setY(A.get()); break;
			case OP_TYA: setA(Y.get()); break;
			case OP_TSX: setX(SP.get()); break;
			case OP_TXS: setSP(X.get()); break;
		}
	}

	@Override
	public MemoryChip getMemory() {
		return mem;
	}

	@Override
	public long getClockCycles() {
		return clock;
	}

	@Override
	public long getNmiAddr() {
		return mem.readUnsigned(0xFFFA) | mem.readUnsigned(0xFFFB) << 8;
	}

	@Override
	public long getPorAddr() {
		return mem.readUnsigned(0xFFFC) | mem.readUnsigned(0xFFFD) << 8;
	}

	@Override
	public long[] getIntAddrs() {
		return new long[] {mem.readUnsigned(0xFFFE) | mem.readUnsigned(0xFFFF) << 8};
	}

	@Override
	public Register getPc() {
		return PC;
	}

	@Override
	public Register getSp() {
		return SP;
	}

	@Override
	public Flags getFlags() {
		return F;
	}

	@Override
	public int getNumRegisters() {
		return 0;
	}

	@Override
	public Register getRegister(int idx) {
		return null;
	}

	@Override
	public int getNumCounters() {
		return 5;
	}

	@Override
	public Register getCounter(int idx) {
		switch (idx) {
			case 0: return PC;
			case 1: return SP;
			case 2: return A;
			case 3: return X;
			case 4: return Y;
			default: throw new RuntimeException("Invalid counter");
		}
	}
}
