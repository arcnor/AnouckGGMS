package com.arcnor.anouckmse.debugger.dialog

import com.arcnor.anouckmse.debugger.Debugger
import com.arcnor.system.VideoChip
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.Graphics
import java.awt.Graphics2D
import javax.swing.JPanel

class DisplayDialog(val debugger: Debugger, val vdp: VideoChip, initialZoom: Int) : DebugDialog(debugger, "Display", false) {
	private val video = object : JPanel() {
		override fun paint(g: Graphics) {
			super.paint(g)

			val preferredSize = preferredSize
			val g2 = g as Graphics2D
			g2.drawImage(vdp.videoSurface, 0, 0, preferredSize.width, preferredSize.height, null)
		}
	}

	init {
		layout = BorderLayout()

		initVideo(initialZoom)

		pack()
		val ownerLoc = debugger.location
		setLocation(ownerLoc.x + debugger.width / 2 - bounds.width / 2, ownerLoc.y - bounds.height)
	}

	private fun initVideo(zoom: Int) {
		val preferredSize = Dimension(vdp.width * zoom, vdp.height * zoom)
		video.preferredSize = preferredSize
		add(video, BorderLayout.CENTER)
	}

	override fun onStep() {

	}

	override fun onGraphicalStep() {
		video.repaint()
	}
}