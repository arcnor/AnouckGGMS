package com.arcnor.cpu

class UnknownOpcodeException : RuntimeException {
	constructor(opcode: Int, pc: Register) : super(String.format("Unknown opcode: 0x%1$02X at PC: 0x%2$08X", opcode.toByte(), pc.get()))

	constructor(opcode: Int, opcode2: Int, pc: Register) : super(String.format("Unknown extended opcode: 0x%1$02X%2$02X at PC: 0x%3$08X", opcode.toByte(), opcode2.toByte(), pc.get()))
}
