package com.arcnor.cpu.z80

import com.arcnor.cpu.Flags

class Intel8080Flags : Flags() {
	companion object {
		private val FLAG_NAMES = arrayOf(
				"C",
				"U1",
				"P",
				"U2",
				"H",
				"U3",
				"Z",
				"S"
		)

		@JvmField
		val FLAG_CARRY = 0
		@JvmField
		val FLAG_UNKNOWN1 = 1
		@JvmField
		val FLAG_PARITY = 2
		@JvmField
		val FLAG_UNKNOWN2 = 3
		@JvmField
		val FLAG_HALFCARRY = 4
		@JvmField
		val FLAG_UNKNOWN3 = 5
		@JvmField
		val FLAG_ZERO = 6
		@JvmField
		val FLAG_SIGN = 7
	}

	override fun getName(index: Int) = FLAG_NAMES[index]
}
