package com.arcnor.anouckmse.debugger.dialog

import com.arcnor.anouckmse.debugger.Debugger
import javax.swing.JDialog

abstract class DebugDialog protected constructor(owner: Debugger, title: String, modal: Boolean) : JDialog(owner, title, modal) {
	abstract fun onStep()
	abstract fun onGraphicalStep()
}
