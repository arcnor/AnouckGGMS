package com.arcnor.anouckmse.debugger

class ASMLine(@JvmField val addr: Int,
              @JvmField val opcode: Int,
              @JvmField val data: Int,
              @JvmField val buffer: ByteArray,
              @JvmField val decoded: String) {
	@JvmField var label: String? = null
}
