package com.arcnor.anouckmse.debugger.command

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog
import java.awt.event.ActionEvent
import java.util.HashMap
import javax.swing.AbstractAction

class WatchCommand(console: ConsoleDialog) : Command(console, "w", "Watch an address for a change in value", object : AbstractAction("Watch Command") {
	override fun actionPerformed(e: ActionEvent) {
		val cmd = e.actionCommand
		val args = cmd.split("\\s+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
		if (args.size != 2) {
			console.append("Watch command takes 1 parameter\n", ConsoleDialog.STYLE_ERROR)
			return
		}
		try {
			val addr = Integer.valueOf(args[1], 16)!!
			val mem = console.debugger.cpu.memory
			val value = mem.readUnsigned(addr)
			WatchCommand.addrs.put(addr, value)
			console.append(String.format("Added watch at %1$04X (current value %2$02X)\n", addr, value), ConsoleDialog.STYLE_INFO)
		} catch (ex: Exception) {
			console.append("Watch command parameter must be a valid hexadecimal address (i.e. 007F)\n", ConsoleDialog.STYLE_ERROR)
		}
	}
}) {
	companion object {
		val addrs: MutableMap<Int, Int> = HashMap()
	}

	override val isStepCommand: Boolean
		get() = true

	override fun onStep() {
		val cpu = console.debugger.cpu
		val pc = cpu.pc
		val mem = cpu.memory
		for ((addr, old) in addrs) {
			val curr = mem.readUnsigned(addr)
			if (curr != old) {
				console.append(String.format("%1$04X - Watch[%2$04X]: %3$02X -> %4$02X\n", pc, addr, old, curr), ConsoleDialog.STYLE_DATA)
				addrs.put(addr, curr)
			}
		}
	}
}
