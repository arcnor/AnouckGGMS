package com.arcnor.cpu

import com.arcnor.toInt

abstract class Flags {
	@JvmField
	protected var flag = BooleanArray(8)

	open fun reset() {
		flag.fill(false)
	}

	operator fun get(index: Int) = flag[index]

	/**
	 * Returns flags as a single byte
	 */
	open fun get(): Int {
		return (flag[7].toInt() shl 7 or
				(flag[6].toInt() shl 6) or
				(flag[5].toInt() shl 5) or
				(flag[4].toInt() shl 4) or
				(flag[3].toInt() shl 3) or
				(flag[2].toInt() shl 2) or
				(flag[1].toInt() shl 1) or
				(flag[0].toInt()))
	}

	open operator fun set(index: Int, value: Boolean) {
		flag[index] = value
	}

	/**
	 * Sets flags with a single byte
	 */
	open fun set(value: Int) {
		flag[7] = value and 0x80 != 0
		flag[6] = value and 0x40 != 0
		flag[5] = value and 0x20 != 0
		flag[4] = value and 0x10 != 0
		flag[3] = value and 0x08 != 0
		flag[2] = value and 0x04 != 0
		flag[1] = value and 0x02 != 0
		flag[0] = value and 0x01 != 0
	}

	abstract fun getName(index: Int): String

	override fun toString(): String {
		val sb = StringBuilder("| ")
		for (j in flag.indices.reversed()) {
			sb.append(if (flag[j]) getName(j) else "-").append(" | ")
		}
		return sb.toString().trim { it <= ' ' }
	}
}
