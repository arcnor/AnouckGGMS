package com.arcnor.anouckmse.machine.chip8

import com.arcnor.cpu.debugger.MachineDebug

class CHIP8MachineDebug : MachineDebug {
	override fun formatLabel(addr: Int): String? {
		return null
	}

	override fun formatReadAddr(addr: Int): String? {
		return null
	}

	override fun formatWriteAddr(addr: Int): String? {
		return null
	}

	override fun commentReadAddr(addr: Int): String? {
		return null
	}

	override fun commentWriteAddr(addr: Int): String? {
		return null
	}
}