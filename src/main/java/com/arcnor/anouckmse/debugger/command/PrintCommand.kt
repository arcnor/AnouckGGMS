package com.arcnor.anouckmse.debugger.command

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog
import java.awt.event.ActionEvent
import javax.swing.AbstractAction

class PrintCommand(console: ConsoleDialog) : Command(console, "p", "Prints register or memory", object : AbstractAction("Print Command") {
	override fun actionPerformed(e: ActionEvent) {
	}
})
