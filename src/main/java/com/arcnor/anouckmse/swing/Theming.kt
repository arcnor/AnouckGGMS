package com.arcnor.anouckmse.swing

import javax.swing.*
import javax.swing.border.CompoundBorder
import javax.swing.plaf.metal.MetalLookAndFeel
import java.awt.*
import java.util.ArrayList

object Theming {
	@JvmStatic
	val COLOR_FORE = Color(169, 183, 198)
	@JvmStatic
	val COLOR_FORE_MENU = Color(186, 186, 186)

	@JvmStatic
	val COLOR_BACK_WINDOW = Color(43, 43, 43)
	@JvmStatic
	val COLOR_BACK_MENU = Color(60, 63, 65)
	@JvmStatic
	val COLOR_BACK_SELECTION_MENU = Color(75, 110, 175)
	@JvmStatic
	val COLOR_BACK_BUTTON_TOP = Color(0x535657)
	@JvmStatic
	val COLOR_BACK_BUTTON_BOTTOM = Color(0x444748)

	@JvmStatic
	val COLOR_BORDER_BUTTON = Color(0x595A5A)
	@JvmStatic
	val COLOR_BORDER_BOTTOM = Color(85, 85, 85)
	@JvmStatic
	val COLOR_BORDER_TOP = Color(40, 40, 40)

	@JvmStatic
	val COLOR_LINE = Color(77, 77, 77)
	@JvmStatic
	val COLOR_SELECTION = Color(33, 66, 131)
	@JvmStatic
	val COLOR_STRING = Color(165, 194, 92)
	@JvmStatic
	val COLOR_FIELD = Color(152, 118, 170)
	@JvmStatic
	val COLOR_RESERVED = Color(204, 120, 50)

	@JvmStatic
	val BORDER_TOP = BorderFactory.createCompoundBorder(
			BorderFactory.createMatteBorder(1, 0, 0, 0, COLOR_BORDER_TOP),
			BorderFactory.createMatteBorder(1, 0, 0, 0, COLOR_BORDER_BOTTOM)
	)
	@JvmStatic
	val BORDER_BUTTON = BorderFactory.createCompoundBorder(
			BorderFactory.createLineBorder(COLOR_BORDER_BUTTON, 1),
			BorderFactory.createEmptyBorder(2, 4, 2, 4)
	)

	@JvmStatic
	fun loadLAF() {
		try {
			UIManager.setLookAndFeel(MetalLookAndFeel::class.java.name)
			val defaults = UIManager.getDefaults()
			val fntMonospaced = Font("Monospaced", Font.PLAIN, 12)
			val fntMenu = Font("Arial", Font.PLAIN, 12)

			val keys = defaults.keys()
			while (keys.hasMoreElements()) {
				val key = keys.nextElement()
				val value = UIManager.get(key)
				if (key != null && key is String && (key.startsWith("Button") || key.startsWith("Menu") || key.startsWith("CheckBoxMenu"))) {
					continue
				}
				if (value != null && value is javax.swing.plaf.FontUIResource) {
					UIManager.put(key, fntMonospaced)
				}
			}
			UIManager.put("ScrollBarUI", AnouckScrollBarUI::class.java.name)
			UIManager.put("PopupMenu.border", BorderFactory.createLineBorder(COLOR_BACK_WINDOW))
			UIManager.put("Panel.background", COLOR_BACK_WINDOW)
			UIManager.put("ScrollPane.border", "")
			UIManager.put("TextField.border", BorderFactory.createCompoundBorder(
					BorderFactory.createLineBorder(COLOR_BORDER_TOP),
					BorderFactory.createEmptyBorder(2, 5, 2, 5))
			)
			UIManager.put("TextField.foreground", COLOR_FORE)
			UIManager.put("TextField.background", COLOR_BACK_MENU)
			UIManager.put("TextField.caretForeground", COLOR_FORE)
			UIManager.put("Button.font", fntMenu)
			val value = ArrayList<Any>()
			value.add(0.5)
			value.add(0.0)
			value.add(COLOR_BACK_BUTTON_TOP)
			value.add(COLOR_BACK_BUTTON_BOTTOM)
			value.add(Color.BLACK)
			UIManager.put("Button.gradient", value)
			UIManager.put("Button.border", BORDER_BUTTON)
			UIManager.put("Button.foreground", COLOR_FORE)
			val brighter = COLOR_BACK_BUTTON_TOP.brighter()
			UIManager.put("Button.select", brighter)
			UIManager.put("Button.shadow", brighter)
			UIManager.put("Button.highlight", brighter)
			UIManager.put("Button.select", brighter)
			UIManager.put("Button.focus", brighter)
			UIManager.put("Label.foreground", COLOR_FORE)
			val menuKeys = arrayOf("Menu", "MenuBar", "MenuItem", "CheckBoxMenuItem", "RadioButtonMenuItem")
			for (menuKey in menuKeys) {
				UIManager.put(menuKey + ".font", fntMenu)
				UIManager.put(menuKey + ".borderPainted", java.lang.Boolean.FALSE)
				UIManager.put(menuKey + ".background", COLOR_BACK_MENU)
				UIManager.put(menuKey + ".foreground", COLOR_FORE_MENU)
				UIManager.put(menuKey + ".selectionBackground", COLOR_BACK_SELECTION_MENU)
				UIManager.put(menuKey + ".selectionForeground", COLOR_FORE_MENU)
			}
		} catch (e: Exception) {
			println("Error setting native LAF: " + e)
		}
	}
}
