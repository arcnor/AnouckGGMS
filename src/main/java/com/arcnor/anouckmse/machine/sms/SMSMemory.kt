package com.arcnor.anouckmse.machine.sms

import com.arcnor.cpu.Register
import com.arcnor.system.MemoryChip
import java.util.Arrays

@Suppress("OVERRIDE_BY_INLINE", "NOTHING_TO_INLINE")
class SMSMemory(mem: ByteArray) : MemoryChip {
	// Memory Frames (4x16K)
	private val frames = Array(4) { ByteArray(0x4000) }
	// ROM pages (number depending on rom size)
	private val pages = Array(Math.max(mem.size / 0x4000, 2)) { ByteArray(0x4000) }
	// Cartridge RAM pages (2x16K)
	private val cartRAMPages = Array(2) { ByteArray(0x4000) }

	init {
		// Default mapping
		frames[0] = pages[0]
		frames[1] = pages[1]

		// Copy RAM
		val nPages = mem.size / 0x4000
		for (n in 0..nPages - 1) {
			System.arraycopy(mem, n * 0x4000, pages[n], 0, 0x4000)
		}
	}

	override fun readSigned(addr: Int) = when {
		addr < 0x400 -> pages[0][addr]              // First 1K, not paged
		addr < 0x4000 -> frames[0][addr]            // ROM page 0 (15K)
		addr < 0x8000 -> frames[1][addr - 0x4000]   // ROM page 1 (16K)
		addr < 0xC000 -> frames[2][addr - 0x8000]   // ROM page 2 or Cart RAM (16K)
		addr < 0xE000 -> frames[3][addr - 0xA000]   // RAM (8K)
		addr < 0x10000 -> frames[3][addr - 0xC000]  // Mirrored RAM (8K)
		else -> 0                                   // Out of bounds
	}

	override fun readUnsigned(addr: Int) = readSigned(addr).toInt() and 0xFF

	override inline fun readSigned(addr: Register) = readSigned(addr.get())
	override inline fun readUnsigned(addr: Register) = readUnsigned(addr.get())

	override fun write(addr: Int, value: Byte) = when {
		addr < 0x400 -> pages[0][addr] = value              // First 1K, not paged
		addr < 0x4000 -> frames[0][addr] = value            // ROM page 0 (15K)
		addr < 0x8000 -> frames[1][addr - 0x4000] = value   // ROM page 1 (16K)
		addr < 0xC000 -> frames[2][addr - 0x8000] = value   // ROM page 2 or Cart RAM (16K)
		addr < 0xE000 -> frames[3][addr - 0xA000] = value   // RAM (8K)
		addr < 0x10000 -> frames[3][addr - 0xC000] = value  // Mirrored RAM (8K)
		else -> {}                                          // Out of bounds
	}

	/**
	 * This will ONLY return continuous memory, and will NOT perform any boundary copy magic
	 */
	override fun getData(addr: Int, length: Int): ByteArray? {
		val src: ByteArray
		val realAddr: Int
		when {
			addr < 0x400 -> {
				src = pages[0]
				realAddr = addr
			}
			addr < 0x4000 -> {
				src = frames[0]
				realAddr = addr
			}
			addr < 0x8000 -> {
				src = frames[1]
				realAddr = addr - 0x4000
			}
			addr < 0xC000 -> {
				src = frames[2]
				realAddr = addr - 0x8000
			}
			addr < 0xE000 -> {
				src = frames[3]
				realAddr = addr - 0xA000
			}
			addr < 0x10000 -> {
				src = frames[3]
				realAddr = addr - 0xC000
			}
			else -> // Out of bounds
				return null
		}
		return Arrays.copyOfRange(src, realAddr, realAddr + length)
	}

	override val regionNames = arrayOf("Z80", "ROM", "RAM", "VRAM")

	override fun getRegionLength(region: Int) = when (region) {
		0 -> 0x8000L
		1 -> (pages.size * 0x4000).toLong()
		2 -> 0x2000L
		3 -> 0x4000L
		else -> 0L
	}

	override fun getRegionOffset(region: Int, offset: Long) = when (region) {
		0 -> offset
		1 -> offset
		2 -> offset + 0x8000L
		3 -> offset + 0xC000L
		else -> 0
	}

	val nFrames: Int
		get() = frames.size

	val nPages: Int
		get() = pages.size

	val nCartRAMPages: Int
		get() = cartRAMPages.size

	override fun getTotalSize() = 0x10000
}
