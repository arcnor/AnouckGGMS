;----------------------------------------------------------------------------
; Mega Drive 6-button controller test
;----------------------------------------------------------------------------

TH_HI                   =       $F5
TH_LO                   =       $D5

                        .org    $0000
                        di
                        im      1
                        ld      sp, $DFF0
                        jp      main

                        .org    $0010
                        ld      a, l
                        out     ($BF), a
                        ld      a, h
                        out     ($BF), a
                        ret

                        .org    $0018
                        ld      a, l
                        out     ($BE), a
                        ld      a, h
                        out     ($BE), a
                        ret

                        .org    $0038
                        in      a, ($BF)
                        call    irq_routine
                        ei
                        reti

                        .org    $0066
                        retn

;----------------------------------------------------------------------------
irq_routine:
                        ; TH = 1 : ?1CBRLDU    3-button pad return value
                        ld      a, TH_HI
                        out     ($3F), a
                        call    delay
                        in      a, ($DC)
                        cpl
                        and     $3F
                        ld      b, a

                        ; TH = 0 : ?0SA00DU    3-button pad return value
                        ld      a, TH_LO
                        out     ($3F), a
                        call    delay
                        in      a, ($DC)
                        cpl
                        rlca
                        rlca
                        and     $C0
                        or      b
                        ld      ($C000), a

                        ; TH = 1 : ?1CBRLDU    3-button pad return value
                        ld      a, TH_HI
                        out     ($3F), a
                        call    delay

                        ; TH = 0 : ?0SA0000    D3-0 are forced to '0'
                        ld      a, TH_LO
                        out     ($3F), a
                        call    delay

                        ; TH = 1 : ?1CBMXYZ    Extra buttons returned in D3-0
                        ld      a, TH_HI
                        out     ($3F), a
                        call    delay
                        in      a, ($DC)
                        cpl
                        and     $0F
                        ld      ($C001), a

                        ; TH = 0 : ?0SA1111    D3-0 are forced to '1'
                        ld      a, TH_LO
                        out     ($3F), a
                        call    delay

                        ; print digits (1)
                        ld      bc, $060D
                        xor     a
                        ld      e, a
                        ld      a, ($C000)
                        ld      d, a
next_digit:             ld      a, d
                        rrca    
                        ld      d, a
                        rlca    
                        and     $01
                        call    printhexd
                        dec     c
                        inc     e
                        ld      a, e
                        cp      8
                        jr      nz, next_digit

                        ; print digits (2)
                        xor     a
                        ld      e, a
                        ld      a, ($C001)
                        ld      d, a
next_digit2:            ld      a, d
                        rrca    
                        ld      d, a
                        rlca    
                        and     $01
                        call    printhexd
                        dec     c
                        inc     e
                        ld      a, e
                        cp      4
                        jr      nz, next_digit2
                        ret

delay:                  nop
                        nop
                        nop
                        nop
                        ret

;----------------------------------------------------------------------------
main:
                        call    vdp_init
                        call    conio_init

                        ; Print text
                        ld      hl, text
                        ld      bc, $0102
                        call    printf

                        ; Enable screen and line ints
                        ld      hl, $81E2
                        rst     10h
                        ei
main_loop:
                        jp      main_loop

;----------------------------------------------------------------------------

                        .include "vdp.inc"
                        .include "conio.inc"

text:                   .db     "6-button pad test",1
                        .db     "by Charles MacDonald",1
                        .db     "E-mail: cgfm2@hotmail.com",1,1
                        .db     "MXYZSACBRLDU",0

sdsc_program_name:      .db     "Mega Drive "
                        .db     "6-Button Controller Test",0
sdsc_release_notes:     .db     "Author: Charles MacDonald",0

                        ; SDSC data

                        .org    $7FE0
                        .db     "SDSC"
                        .db     $01, $00
                        .db     $30
                        .db     $07
                        .db     $01, $20
                        .db     $00, $00
                        .dw     sdsc_program_name
                        .dw     sdsc_release_notes

                        ; SEGA data

                        .org    $7FF0
                        .db     "TMR SEGA"
                        .dw     $FFFF
                        .db     $00, $00
                        .db     $00, $00
                        .db     $00
                        .db     $00

                        .end

