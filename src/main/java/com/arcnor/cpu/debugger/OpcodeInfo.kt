package com.arcnor.cpu.debugger

data class OpcodeInfo(
		@JvmField
		var opcodeSize: Int = 0,
		@JvmField
		var dataSize: Int = 0
)
