package com.arcnor.anouckmse.debugger.command

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog
import javax.swing.JPanel

abstract class StepCommand(console: ConsoleDialog, name: String, desc: String, actionKey: String) : Command(console, name, desc, (console.debugger.contentPane as JPanel).actionMap.get(actionKey))
