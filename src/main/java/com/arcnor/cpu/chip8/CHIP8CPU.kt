package com.arcnor.cpu.chip8

import com.arcnor.cpu.Flags
import com.arcnor.cpu.Register16
import com.arcnor.cpu.Register8
import com.arcnor.system.CPUChip
import com.arcnor.system.MemoryChip
import com.arcnor.vdp.CHIP8Graphics
import java.util.Random
import java.util.Stack
import kotlin.coroutines.experimental.EmptyCoroutineContext.plus

@Suppress("NOTHING_TO_INLINE")
class CHIP8CPU(override val memory: MemoryChip, val vdp: CHIP8Graphics) : CPUChip {
	companion object {
		val DEBUG_IDX_PC = 0
		val DEBUG_IDX_SP = 1
	}

	private var _clock = 0L

	override val clockCycles: Long
		get() = _clock
	override val nmiAddr = 0x200L
	override val porAddr = 0x200L
	override val intAddrs = longArrayOf()

	override val pc = Register16("PC")
	override val sp = Register16("SP")

	override val flags: Flags
		get() = CHIP8Flags()
	override val numRegisters = 17

	// FIXME: This should not be used, but a real stack in RAM (so it can be debugged)
	private val _stack = Stack<Int>()

	private val regV = Array(16) { idx -> Register8("V${Integer.toHexString(idx).toUpperCase()}") }
	private val regI = Register16("I")

	private val rnd = Random(0x1234198542)

	init {
		reset()
	}

	override fun reset() {
		pc.set(0x200)
		sp.set(0)
	}

	override fun doCycle() {
		val opcode = fetch()

		decodeOp(opcode)
	}

	private fun fetch(): Int {
		val op = memory.readUnsigned(pc.inc().get())
		val op2 = memory.readUnsigned(pc.inc().get())

		++_clock

		return op shl 8 or op2
	}

	private inline fun decodeOp(opcode: Int) {
		val category = opcode and 0xF000
		val x = opcode and 0x0F00 ushr 8
		val y = opcode and 0x00F0 ushr 4
		val kk = (opcode and 0x00FF)

		when (category) {
			0x0000 -> {
				when (opcode) {
					0x00E0 -> vdp.clear()
					0x00EE -> pc.set(_stack.pop())
					else -> throwError(opcode)
				}
			}
			0x1000 -> { // JP addr
				val addr = opcode and 0x0FFF
				pc.set(addr)
			}
			0x2000 -> { // 2nnn - CALL addr
				_stack.push(pc.get())
				pc.set(opcode and 0x0FFF)
			}
			0x3000 -> { // 3xkk - SE Vx, byte
				if (regV[x].get() == kk) {
					pc.set(pc.get() + 2)
				}
			}
			0x4000 -> { // 4xkk - SNE Vx, byte
				if (regV[x].get() != kk) {
					pc.set(pc.get() + 2)
				}
			}
			0x6000 -> { // 6xkk - LD Vx, byte
				regV[x].set(kk and 0xFF)
			}
			0x7000 -> { // 7xkk - ADD Vx, byte
				regV[x].set((regV[x].get() + kk) and 0xFF)
			}
			0x8000 -> {
				when (opcode and 0x000F) {
					0x00 -> {   // 8xy0 - LD Vx, Vy
						regV[x].set(regV[y].get())
					}
					0x01 -> {   // 8xy1 - OR Vx, Vy
						regV[x].set(regV[x].get() or regV[y].get())
					}
					0x02 -> {   // 8xy2 - AND Vx, Vy
						regV[x].set(regV[x].get() and regV[y].get())
					}
					0x03 -> {    // 8xy3 - XOR Vx, Vy
						regV[x].set(regV[x].get() xor regV[y].get())
					}
					0x04 -> {    // 8xy4 - ADD Vx, Vy
						val result = regV[x].get() + regV[y].get()
						regV[x].set(result and 0xFF)
						regV[0xF].set(if (result > 0xFF) 1 else 0)
					}
					0x05 -> {    // 8xy5 - SUB Vx, Vy
						if (regV[x].get() > regV[y].get()) {
							regV[0xF].set(1)
						} else {
							regV[0xF].set(0)
						}
						regV[x].set((regV[x].get() - regV[y].get()) and 0xFF)
					}
					0x06 -> {   // 8xy6 - SHR Vx {, Vy}
						val old = regV[x].get()
						regV[0xF].set(old and 0x01)
						regV[x].set(old shr 1)
					}
					else -> throwError(opcode)
				}
			}
			0xA000 -> { // LD I, addr
				regI.set(opcode and 0x0FFF)
			}
			0xC000 -> { // Cxkk - RND Vx, byte
				regV[x].set((rnd.nextInt(0x100) and kk) and 0xFF)
			}
			0xD000 -> { // Dxyn - DRW Vx, Vy, nibble
				regV[0xF].set(0)
				val posX = regV[x].get()
				val posY = regV[y].get()
				val height = opcode and 0x000F
				var row = 0
				while (row < height) {
					val currRow = memory.readUnsigned(row + regI.get())
					var pixelOffset = 0
					while (pixelOffset < 8) {
						val loc = posX + pixelOffset + ((posY + row) * 64)
						++pixelOffset
						if ((posY + row) >= 32 || (posX + pixelOffset - 1) >= 64) {
							// ignore pixels outside the screen
							continue
						}
						val mask = 1 shl 8 - pixelOffset
						val currPixel = (currRow and mask) shr (8 - pixelOffset)
						if (!vdp.xor(loc, currPixel)) {
							regV[0xF].set(1)
						}
					}
					++row
				}
			}
			0xF000 -> {
				when (opcode and 0x00FF) {
					0x1E -> { // Fx1E - ADD I, Vx
						regI.set((regI.get() + regV[x].get()) and 0xFFFF)
					}
					0x33 -> { // Fx33 - LD B, Vx
						val str = regV[x].get().toString().padStart(3, '0')
						val addr = regI.get()
						memory.write(addr, (str[0] - '0').toByte())
						memory.write(addr + 1, (str[1] - '0').toByte())
						memory.write(addr + 2, (str[2] - '0').toByte())
					}
					0x65 -> { // Fx65 - LD Vx, [I]
						val addr = regI.get()
						(0..x).forEach { j ->
							regV[j].set(memory.readUnsigned(addr + j))
						}
					}
					else -> throwError(opcode)
				}
			}
			else -> throwError(opcode)
		}
	}

	private fun throwError(opcode: Int) {
		throw UnsupportedOperationException("Unknown opcode 0x${Integer.toHexString(opcode and 0xFFFF).toUpperCase()} at ROM address 0x${Integer.toHexString(pc.get() - 0x200 - 2)}")
	}

	override fun getRegister(idx: Int) = when {
		idx < 16 -> regV[idx]
		idx == 16 -> regI
		else -> throw RuntimeException("Invalid register index $idx")
	}

	override val numCounters = 2

	override fun getCounter(idx: Int) = when (idx) {
		DEBUG_IDX_PC -> pc
		DEBUG_IDX_SP -> sp
		else -> throw RuntimeException("Invalid counter $idx")
	}
}
