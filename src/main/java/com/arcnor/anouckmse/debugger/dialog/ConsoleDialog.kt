package com.arcnor.anouckmse.debugger.dialog

import com.arcnor.anouckmse.debugger.Debugger
import com.arcnor.anouckmse.debugger.command.ClockCommand
import com.arcnor.anouckmse.debugger.command.Command
import com.arcnor.anouckmse.debugger.command.ContinueCommand
import com.arcnor.anouckmse.debugger.command.HelpCommand
import com.arcnor.anouckmse.debugger.command.LogCommand
import com.arcnor.anouckmse.debugger.command.PrintCommand
import com.arcnor.anouckmse.debugger.command.StepIntoCommand
import com.arcnor.anouckmse.debugger.command.StepOutCommand
import com.arcnor.anouckmse.debugger.command.StepOverCommand
import com.arcnor.anouckmse.debugger.command.WatchCommand
import com.arcnor.anouckmse.swing.Theming
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Insets
import java.awt.event.ActionEvent
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.lang.reflect.InvocationTargetException
import java.util.ArrayList
import java.util.TreeMap
import javax.swing.AbstractAction
import javax.swing.Action
import javax.swing.BorderFactory
import javax.swing.JPanel
import javax.swing.JScrollPane
import javax.swing.JTextField
import javax.swing.JTextPane
import javax.swing.text.BadLocationException
import javax.swing.text.DefaultCaret
import javax.swing.text.StyleConstants

class ConsoleDialog(val debugger: Debugger) : DebugDialog(debugger, "Console", false) {
	companion object {
		const val STYLE_USER = "user"
		const val STYLE_ERROR = "error"
		const val STYLE_INFO = "info"
		const val STYLE_DATA = "data"

		private val COLOR_FORE_USER = Color.WHITE
		private val COLOR_FORE_ERROR = Color.RED
		private val COLOR_FORE_INFO = Color(99, 169, 104)
		private val COLOR_FORE_DATA = Color.YELLOW
	}

	private val txtConsole: JTextPane
	private val actCmd: Action

	private val commands = TreeMap<String, Command>()
	private val stepCommands = ArrayList<Command>()

	init {
		layout = BorderLayout()
		txtConsole = JTextPane()
		txtConsole.background = debugger.background
		txtConsole.foreground = Theming.COLOR_FORE
		txtConsole.isEditable = false
		txtConsole.margin = Insets(4, 4, 4, 4)

		val caret = txtConsole.caret as DefaultCaret
		caret.updatePolicy = DefaultCaret.ALWAYS_UPDATE

		createStyles(txtConsole)
		add(JScrollPane(txtConsole), BorderLayout.CENTER)

		val pnlInput = JPanel(BorderLayout())
		pnlInput.border = Theming.BORDER_TOP
		pnlInput.background = Theming.COLOR_BACK_MENU

		add(pnlInput, BorderLayout.SOUTH)

		fillCommandMap()
		actCmd = createCommandAction()
		val txtCmdInput = JTextField()
		txtCmdInput.action = actCmd
		val pnlCmdInputContainer = JPanel(BorderLayout())
		pnlCmdInputContainer.border = BorderFactory.createEmptyBorder(5, 5, 5, 5)
		pnlCmdInputContainer.add(txtCmdInput)
		pnlInput.add(pnlCmdInputContainer, BorderLayout.CENTER)

		setSize(550, debugger.height / 2)
		val ownerLoc = debugger.location
		setLocation((ownerLoc.getX() - bounds.getWidth()).toInt(), ownerLoc.getY().toInt())

		append("Console ready for action\nType ? to get a list of commands\n\n")
		this.addWindowFocusListener(object : WindowAdapter() {
			override fun windowGainedFocus(e: WindowEvent?) {
				txtCmdInput.requestFocusInWindow()
			}
		})
	}

	private fun fillCommandMap() {
		val clazzez = arrayOf(
				HelpCommand::class.java,
				StepIntoCommand::class.java,
				StepOverCommand::class.java,
				StepOutCommand::class.java,
				ContinueCommand::class.java,
				ClockCommand::class.java,
				PrintCommand::class.java,
				LogCommand::class.java,
				WatchCommand::class.java
		)

		for (clazz in clazzez) {
			try {
				val constructor = clazz.getConstructor(ConsoleDialog::class.java)
				val cmd = constructor.newInstance(this)
				commands.put(cmd.name, cmd)
				if (cmd.isStepCommand) {
					stepCommands.add(cmd)
				}
			} catch (e: NoSuchMethodException) {
				e.printStackTrace()
			} catch (e: InvocationTargetException) {
				e.printStackTrace()
			} catch (e: InstantiationException) {
				e.printStackTrace()
			} catch (e: IllegalAccessException) {
				e.printStackTrace()
			}
		}
	}

	private fun createStyles(txtPane: JTextPane) {
		val stlUser = txtPane.addStyle(STYLE_USER, null)
		StyleConstants.setForeground(stlUser, COLOR_FORE_USER)

		val stlError = txtPane.addStyle(STYLE_ERROR, null)
		StyleConstants.setForeground(stlError, COLOR_FORE_ERROR)

		val stlInfo = txtPane.addStyle(STYLE_INFO, null)
		StyleConstants.setForeground(stlInfo, COLOR_FORE_INFO)

		val stlData = txtPane.addStyle(STYLE_DATA, null)
		StyleConstants.setForeground(stlData, COLOR_FORE_DATA)
	}

	private fun createCommandAction(): Action {
		val result = object : AbstractAction("Process Command") {
			override fun actionPerformed(e: ActionEvent) {
				val txt = e.source as JTextField
				val cmdStr = txt.text.trim { it <= ' ' }
				if (cmdStr.isEmpty()) {
					return
				}
				txt.text = ""
				append("> " + cmdStr + "\n", STYLE_USER)
				val cmd0 = cmdStr.split("\\s+".toRegex()).dropLastWhile(String::isEmpty).toTypedArray()[0]
				val cmd = commands[cmd0.toLowerCase()]
				if (cmd == null) {
					append("Unknown command\n", STYLE_ERROR)
					return
				}
				cmd.action.actionPerformed(ActionEvent(this, ActionEvent.ACTION_PERFORMED, cmdStr))
			}
		}

		return result
	}

	@JvmOverloads fun append(line: String, style: String? = null) {
		val doc = txtConsole.document
		val stl = when {
			style != null -> txtConsole.getStyle(style)
			else -> null
		}
		try {
			doc.insertString(doc.length, line, stl)
		} catch (e: BadLocationException) {
			e.printStackTrace()
		}
	}

	fun getCommands(): Iterable<Command> {
		return commands.values
	}

	override fun onStep() {
		for (j in stepCommands.indices) {
			stepCommands[j].onStep()
		}
	}

	override fun onGraphicalStep() {
		// Do nothing, yet...
	}
}
