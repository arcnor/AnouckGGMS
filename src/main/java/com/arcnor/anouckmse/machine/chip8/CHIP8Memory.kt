package com.arcnor.anouckmse.machine.chip8

import com.arcnor.cpu.Register
import com.arcnor.system.MemoryChip
import java.util.Arrays

@Suppress("OVERRIDE_BY_INLINE", "NOTHING_TO_INLINE")
class CHIP8Memory(rom: ByteArray) : MemoryChip {
	private val mem = ByteArray(0x1000)

	init {
		System.arraycopy(CHIP8CharMap, 0, mem, 0x0, CHIP8CharMap.size)
		System.arraycopy(rom, 0, mem, 0x200, rom.size)
	}

	override fun readSigned(addr: Int) = mem[addr]

	override fun readUnsigned(addr: Int) = mem[addr].toInt() and 0xFF

	override inline fun readSigned(addr: Register) = readSigned(addr.get())
	override inline fun readUnsigned(addr: Register) = readUnsigned(addr.get())

	override fun write(addr: Int, value: Byte) {
		mem[addr] = value
	}

	override fun getData(addr: Int, length: Int) = Arrays.copyOfRange(mem, addr, addr + length)

	override val regionNames = arrayOf("FONT", "RESERVED", "DATA")

	override fun getRegionLength(region: Int) = when (region) {
		0 -> 0x50L
		1 -> 0x200 - 0x50L
		2 -> 0x1000 - 0x200L
		else -> throw RuntimeException("Unknown region")
	}

	override fun getRegionOffset(region: Int, offset: Long) = when (region) {
		0 -> 0x00L
		1 -> 0x50L
		2 -> 0x200L
		else -> throw RuntimeException("Unknown region")
	}

	override fun getTotalSize() = mem.size
}