package com.arcnor.cpu.z80

import com.arcnor.cpu.Flags
import com.arcnor.cpu.Register16

class Z80Register(name: String, private val f: Flags) : Register16(name) {
	// FIXME: Set flags
	fun incH() = h++
	fun decH() = h--
	fun incL() = l++
	fun decL() = l--
}
