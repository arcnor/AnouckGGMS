package com.arcnor.cpu.gameboy

import com.arcnor.cpu.intel8080.Intel8080
import com.arcnor.system.MemoryChip

class LR35902(memory: MemoryChip) : Intel8080(memory) {
	companion object {
		// Bit Set, Reset, and Test Group
		val OP_BIT_REG = 0xCB
	}

	override val intAddrs = longArrayOf(
			0x0000, 0x0008, 0x0010, 0x0018, 0x0020, 0x0028, 0x0030, 0x0038, // – For RST instruction of CPU.
			0x0040, 0x0048, 0x0050, 0x0058, 0x0060 // Interrupt Vectors (VBL,LCD,Timer,Serial,Joypad)
	)


	override val flags = LR35902Flags()
}
