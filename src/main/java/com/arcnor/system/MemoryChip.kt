package com.arcnor.system

import com.arcnor.cpu.Register

interface MemoryChip {
	fun readSigned(addr: Int): Byte
	fun readUnsigned(addr: Int): Int

	fun readSigned(addr: Register): Byte
	fun readUnsigned(addr: Register): Int

	fun write(addr: Int, value: Byte)

	fun getData(addr: Int, length: Int): ByteArray?

	/**
	 * The name of the memory regions in this chip
	 */
	val regionNames: Array<String>

	/**
	 * The size of the specified region
	 */
	fun getRegionLength(region: Int): Long

	/**
	 * The offset relative to the specified region
	 */
	fun getRegionOffset(region: Int, offset: Long): Long

	/**
	 * The total size of the memory, from 0x00 to the end
	 */
	fun getTotalSize() : Int
}
