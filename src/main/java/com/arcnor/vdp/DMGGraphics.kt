package com.arcnor.vdp

import com.arcnor.system.VideoChip
import java.awt.Color

class DMGGraphics : VideoChip(160, 144) {
	private val PALETTE = arrayOf(
			Color(224, 248, 208),
			Color(146, 192, 112),
			Color(52, 104, 86),
			Color(8, 24, 32)
	)

	private val mem = ByteArray(width * height)

	init {
		g2.color = PALETTE[0]
		g2.fillRect(0, 0, width, height)
	}
}