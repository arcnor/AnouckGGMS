package com.arcnor.cpu.intel8080

import com.arcnor.cpu.debugger.CPUDebug
import com.arcnor.cpu.debugger.JumpType
import com.arcnor.cpu.debugger.OpcodeInfo

open class Intel8080Debug<in C: Intel8080> : CPUDebug<C> {
	protected val opcodeInfo = OpcodeInfo()

	override fun decodeOpcodeInfo(opcode: Int): OpcodeInfo {
		opcodeInfo.opcodeSize = 1
		opcodeInfo.dataSize = 0



		return opcodeInfo
	}

	override fun opcodeJumpType(opcode: Int, data: Int): JumpType {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	override fun opcodeJumpData(opcode: Int, data: Int): Int {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	override fun opToStr(oldPC: Int, opcode: Int, op: Int, op2: Int, data: Int, cpu: C): String {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	override fun formatLabel(addr: Int): String {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}
}