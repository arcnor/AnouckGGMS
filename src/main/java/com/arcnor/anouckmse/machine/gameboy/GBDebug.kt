package com.arcnor.anouckmse.machine.gameboy

import com.arcnor.cpu.debugger.MachineDebug

class GBDebug : MachineDebug {
	override fun formatLabel(addr: Int) = null

	override fun formatReadAddr(addr: Int) = null

	override fun formatWriteAddr(addr: Int) = null

	override fun commentReadAddr(addr: Int) = null

	override fun commentWriteAddr(addr: Int) = null
}