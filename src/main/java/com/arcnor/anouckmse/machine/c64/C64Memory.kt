package com.arcnor.anouckmse.machine.c64

import com.arcnor.cpu.Register
import com.arcnor.system.MemoryChip

import java.util.Arrays
import kotlin.experimental.and

@Suppress("OVERRIDE_BY_INLINE", "NOTHING_TO_INLINE")
class C64Memory(chargen: ByteArray, basic: ByteArray, kernal: ByteArray) : MemoryChip {
	private companion object {
		val OFFS_BIOS_RAM = 0x0002L
		val SIZE_BIOS_RAM = 0x00FEL
		val OFFS_STACK = 0x0100L
		val SIZE_STACK = 0x0100L
		val OFFS_BIOS_DATA = 0x0200L
		val SIZE_BIOS_DATA = 0x0600L
		val OFFS_BASIC_RAM = 0x0800L
		val SIZE_BASIC_RAM = 0x7800L
		val OFFS_BASIC = 0xA000L
		val SIZE_BASIC = 0x2000L
		val OFFS_RAM = 0xC000L
		val SIZE_RAM = 0x1000L
		val OFFS_CHAR = 0xD000L
		val SIZE_CHAR = 0x1000L
		val OFFS_KERNAL = 0xE000L
		val SIZE_KERNAL = 0x2000L
	}
	
	private val mem: ByteArray

	init {
		if (chargen.size.toLong() != SIZE_CHAR) {
			throw RuntimeException("Invalid chargen size")
		}
		if (basic.size.toLong() != SIZE_BASIC) {
			throw RuntimeException("Invalid basic size")
		}
		if (kernal.size.toLong() != SIZE_KERNAL) {
			throw RuntimeException("Invalid kernal size")
		}

		mem = ByteArray(0x10000)
		System.arraycopy(chargen, 0, this.mem, OFFS_CHAR.toInt(), chargen.size)
		System.arraycopy(basic, 0, this.mem, OFFS_BASIC.toInt(), basic.size)
		System.arraycopy(kernal, 0, this.mem, OFFS_KERNAL.toInt(), kernal.size)
	}

	override fun readSigned(addr: Int) = mem[addr]

	override fun readUnsigned(addr: Int) = mem[addr].toInt() and 0xFF

	override inline fun readSigned(addr: Register) = readSigned(addr.get())
	override inline fun readUnsigned(addr: Register) = readUnsigned(addr.get())

	override fun write(addr: Int, value: Byte) {
		mem[addr] = value
	}

	override fun getData(addr: Int, length: Int) = Arrays.copyOfRange(mem, addr, addr + length)

	override val regionNames = arrayOf("BIOS RAM", "STACK", "BIOS DATA", "BASIC RAM", "BASIC", "RAM", "CHAR", "KERNAL")

	override fun getRegionLength(region: Int): Long = when (region) {
		0 -> SIZE_BIOS_RAM
		1 -> SIZE_STACK
		2 -> SIZE_BIOS_DATA
		3 -> SIZE_BASIC_RAM
		4 -> SIZE_BASIC
		5 -> SIZE_RAM
		6 -> SIZE_CHAR
		7 -> SIZE_KERNAL
		else -> -1L
	}

	override fun getRegionOffset(region: Int, offset: Long) = when (region) {
		0 -> OFFS_BIOS_RAM
		1 -> OFFS_STACK
		2 -> OFFS_BIOS_DATA
		3 -> OFFS_BASIC_RAM
		4 -> OFFS_BASIC
		5 -> OFFS_RAM
		6 -> OFFS_CHAR
		7 -> OFFS_KERNAL
		else -> -1
	}

	override fun getTotalSize() = mem.size
}
