package com.arcnor.cpu.z80

import com.arcnor.cpu.Flags

class Z80Flags : Flags() {
	companion object {
		private val FLAG_NAMES = arrayOf(
				"C",
				"N",
				"P",
				"X",
				"H",
				"Y",
				"Z",
				"S"
		)

		@JvmField
		val FLAG_CARRY = 0
		@JvmField
		val FLAG_NEG = 1
		@JvmField
		val FLAG_PARITY = 2
		@JvmField
		val FLAG_X = 3
		@JvmField
		val FLAG_HALFCARRY = 4
		@JvmField
		val FLAG_Y = 5
		@JvmField
		val FLAG_ZERO = 6
		@JvmField
		val FLAG_SIGN = 7
	}

	override fun getName(index: Int) = FLAG_NAMES[index]
}
