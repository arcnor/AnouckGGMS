package com.arcnor

import com.arcnor.cpu.Register
import com.arcnor.system.MemoryChip
import java.util.Arrays

@Suppress("OVERRIDE_BY_INLINE", "NOTHING_TO_INLINE")
class SimpleMemory(size: Int) : MemoryChip {
	private val mem = ByteArray(size)

	override val regionNames = arrayOf("MEM")

	override fun readSigned(addr: Int) = mem[addr]

	override fun readUnsigned(addr: Int) = mem[addr].toInt() and 0xFF

	override inline fun readSigned(addr: Register) = readSigned(addr.get())
	override inline fun readUnsigned(addr: Register) = readUnsigned(addr.get())

	override fun write(addr: Int, value: Byte) {
		mem[addr] = value
	}

	override fun getData(addr: Int, length: Int) = Arrays.copyOfRange(mem, addr, addr + length)

	override fun getRegionLength(region: Int) = mem.size.toLong()

	override fun getRegionOffset(region: Int, offset: Long) = offset

	override fun getTotalSize() = mem.size
}
