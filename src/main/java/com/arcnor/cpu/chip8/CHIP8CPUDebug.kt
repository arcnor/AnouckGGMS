package com.arcnor.cpu.chip8

import com.arcnor.cpu.debugger.CPUDebug
import com.arcnor.cpu.debugger.JumpType
import com.arcnor.cpu.debugger.MachineDebug
import com.arcnor.cpu.debugger.OpcodeInfo
import com.arcnor.cpu.debugger.ROMDebug

class CHIP8CPUDebug(private val machineDebug: MachineDebug, private val romDebug: ROMDebug?) : CPUDebug<CHIP8CPU> {
	private val opcodeInfo = OpcodeInfo()
	init {
		opcodeInfo.dataSize = 0
		opcodeInfo.opcodeSize = 2
	}

	override fun decodeOpcodeInfo(opcode: Int): OpcodeInfo {
		return opcodeInfo
	}

	override fun opcodeJumpType(opcode: Int, data: Int): JumpType {
		val op1 = opcode and 0xF000
		when (op1) {
			0x0000 -> {
				when (opcode) {
					0x0000 -> return JumpType.HALT
					0x00EE -> return JumpType.RET
				}
			}
			0x1000 -> return JumpType.JUMP
			0x2000 -> return JumpType.CALL
			0x3000, 0x4000, 0x5000, 0xE000 -> return JumpType.JUMP_REL_COND
		}
		return JumpType.NO_JUMP
	}

	override fun opcodeJumpData(opcode: Int, data: Int): Int {
		val category = opcode and 0xF000
		val opData = opcode and 0x0FFF
		return when (category) {
			0x1000, 0x2000 -> opData
			0x3000, 0x4000, 0x5000, 0xE000 -> 2 // Skip next instruction if...
			else -> -1
		}
	}

	override fun opToStr(oldPC: Int, opcode: Int, op: Int, op2: Int, data: Int, cpu: CHIP8CPU): String {
		val category = opcode and 0xF000
		val x = (opcode and 0x0F00) shr 8
		val y = (opcode and 0x00F0) shr 4
		val kk = opcode and 0x00FF
		val addr = opcode and 0x0FFF
		var opName = "UNKNOWN"
		var comment = ""
		when (category) {
			0x0000 -> {
				when (opcode) {
					0x00E0 -> {
						opName = String.format("CLR")
						comment = "Clears the display"
					}
					0x00EE -> {
						opName = String.format("RET")
						comment = "Returns from a subroutine"
					}
				}
			}
			0x1000 -> {
				opName = String.format("JP 0x%1$02X", addr)
			}
			0x2000 -> {
				opName = String.format("CALL 0x%1$02X", addr)
			}
			0x3000 -> {
				opName = String.format("SE V%1\$02X, 0x%2$02X", x, kk)
				comment = String.format("Skip next if V%1\$01X == 0x%2$02X", x, kk)
			}
			0x4000 -> {
				opName = String.format("SNE V%1\$01X, 0x%2$02X", x, kk)
				comment = String.format("Skip next if V%1\$01X != 0x%2$02X", x, kk)
			}
			0x6000 -> {
				opName = String.format("LD V%1\$01X, 0x%2$02X", x, kk)
				comment = String.format("V%1\$01X = 0x%2$02X", x, kk)
			}
			0x7000 -> {
				opName = String.format("ADD V%1\$01X, 0x%2$02X", x, kk)
				comment = String.format("V%1\$01X += 0x%2$02X", x, kk)
			}
			0x8000 -> {
				val subCategory = opcode and 0x000F
				when (subCategory) {
					0 -> {
						opName = String.format("LD V%1\$01X, V%2\$01X", x, y)
						comment = String.format("V%1\$01X = V%2\$01X", x, y)
					}
					1 -> {
						opName = String.format("OR V%1\$01X, V%2\$01X", x, y)
						comment = String.format("V%1\$01X = V%1\$01X OR V%2\$01X", x, y)
					}
					2 -> {
						opName = String.format("AND V%1\$01X, V%2\$01X", x, y)
						comment = String.format("V%1\$01X = V%1\$01X AND V%2\$01X", x, y)
					}
					3 -> {
						opName = String.format("XOR V%1\$01X, V%2\$01X", x, y)
						comment = String.format("V%1\$01X = V%1\$01X XOR V%2\$01X", x, y)
					}
					4 -> {
						opName = String.format("ADD V%1\$01X, V%2\$01X", x, y)
						comment = String.format("V%1\$01X += V%2\$01X, VF = carry", x, y)
					}
					5 -> {
						opName = String.format("SUB V%1\$01X, V%2\$01X", x, y)
						comment = String.format("V%1\$01X -= V%2\$01X, VF = NOT borrow", x, y)
					}
				}
			}
			0xA000 -> {
				opName = String.format("LD I, 0x%1$02X", addr)
				comment = String.format("I = 0x%1$02X", addr)
			}
			0xC000 -> {
				opName = String.format("RND V%1\$01X, 0x%2$02X", x, kk)
				comment = String.format("V%1\$01X = Random(0 - 255) AND 0x%2$02X", x, kk)
			}
			0xD000 -> {
				val n = opcode and 0x000F
				opName = String.format("DRW V%1\$01X, V%2\$01X, 0x%3$02X", x, y, n)
				comment = String.format("Display %1\$d-byte sprite starting from I at (V%2\$02X, V%3\$02X)", n, x, y)
			}
			0xF000 -> {
				val subCategory = opcode and 0x00FF
				when (subCategory) {
					0x1E -> {
						opName = String.format("ADD I, V%1\$01X", x)
						comment = String.format("I += V%1\$01X", x)
					}
					0x33 -> {
						opName = String.format("LD B, V%1\$01X", x)
						comment = String.format("Write mem at [I, I + 1, I + 2] with BCD of V%1\$01X", x)
					}
					0x65 -> {
						opName = String.format("LD V%1\$01X, [I]", x)
						comment = String.format("Set V0 to V%1\$01X to memory values from I to I + %1\$d", x, x)
					}
				}
			}
		}
		return String.format("%1$-20s; %2\$s", opName, comment)
	}

	override fun formatLabel(addr: Int): String {
		var lbl: String? = romDebug?.formatLabel(addr)
		if (lbl == null) {
			lbl = machineDebug.formatLabel(addr)
		}
		return if (lbl != null) lbl else String.format("L%1$04X", addr)
	}
}