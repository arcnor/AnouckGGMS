package com.arcnor.cpu.z80;

import com.arcnor.cpu.Flags;
import com.arcnor.cpu.Register;
import com.arcnor.cpu.Register16;
import com.arcnor.cpu.UnknownOpcodeException;
import com.arcnor.system.CPUChip;
import com.arcnor.system.MemoryChip;

public class Z80 implements CPUChip {
	private int clock, counter;
	private MemoryChip mem;

	// Registers and flags
	private Flags F = new Z80Flags();
	private Z80Register AF = new Z80Register("AF", F), BC = new Z80Register("BC", F), DE = new Z80Register("DE", F), HL = new Z80Register("HL", F);
	private Register IX = new Register16("IX"), IY = new Register16("IY"), PC = new Register16("PC"), SP = new Register16("SP");
	private Register I, R;

	public static final int DEBUG_IDX_PC = 0;
	public static final int DEBUG_IDX_SP = 1;
	public static final int DEBUG_IDX_IX = 2;
	public static final int DEBUG_IDX_IY = 3;

	private boolean IFF1, IFF2;
	// Interrupt mode
	private int IM;

	private Z80Alu alu = new Z80Alu(F);

	// Opcodes

	// 8-Bit Load Group
	static final int OP_LD_A_VAL  = 0x3E;
	static final int OP_LD_B_VAL  = 0x06;
	static final int OP_LD_C_VAL  = 0x0E;
	static final int OP_LD_D_VAL  = 0x16;
	static final int OP_LD_E_VAL  = 0x1E;
	static final int OP_LD_H_VAL  = 0x26;
	static final int OP_LD_L_VAL  = 0x2E;

	static final int OP_LD_HL_ADDR = 0x36;

	static final int OP_LD_A_A = 0x7F;
	static final int OP_LD_A_B = 0x78;
	static final int OP_LD_A_C = 0x79;
	static final int OP_LD_A_D = 0x7A;
	static final int OP_LD_A_E = 0x7B;
	static final int OP_LD_A_H = 0x7C;
	static final int OP_LD_A_L = 0x7D;
	static final int OP_LD_A_HL_ADDR = 0x7E;

	static final int OP_LD_B_A = 0x47;
	static final int OP_LD_B_B = 0x40;
	static final int OP_LD_B_C = 0x41;
	static final int OP_LD_B_D = 0x42;
	static final int OP_LD_B_E = 0x43;
	static final int OP_LD_B_H = 0x44;
	static final int OP_LD_B_L = 0x45;
	static final int OP_LD_B_HL_ADDR = 0x46;

	static final int OP_LD_C_A = 0x4F;
	static final int OP_LD_C_B = 0x48;
	static final int OP_LD_C_C = 0x49;
	static final int OP_LD_C_D = 0x4A;
	static final int OP_LD_C_E = 0x4B;
	static final int OP_LD_C_H = 0x4C;
	static final int OP_LD_C_L = 0x4D;
	static final int OP_LD_C_HL_ADDR = 0x4E;

	static final int OP_LD_D_A = 0x57;
	static final int OP_LD_D_B = 0x50;
	static final int OP_LD_D_C = 0x51;
	static final int OP_LD_D_D = 0x52;
	static final int OP_LD_D_E = 0x53;
	static final int OP_LD_D_H = 0x54;
	static final int OP_LD_D_L = 0x55;
	static final int OP_LD_D_HL_ADDR = 0x56;

	static final int OP_LD_E_A = 0x5F;
	static final int OP_LD_E_B = 0x58;
	static final int OP_LD_E_C = 0x59;
	static final int OP_LD_E_D = 0x5A;
	static final int OP_LD_E_E = 0x5B;
	static final int OP_LD_E_H = 0x5C;
	static final int OP_LD_E_L = 0x5D;
	static final int OP_LD_E_HL_ADDR = 0x5E;

	static final int OP_LD_H_A = 0x67;
	static final int OP_LD_H_B = 0x60;
	static final int OP_LD_H_C = 0x61;
	static final int OP_LD_H_D = 0x62;
	static final int OP_LD_H_E = 0x63;
	static final int OP_LD_H_H = 0x64;
	static final int OP_LD_H_L = 0x65;
	static final int OP_LD_H_HL_ADDR = 0x66;

	static final int OP_LD_L_A = 0x6F;
	static final int OP_LD_L_B = 0x68;
	static final int OP_LD_L_C = 0x69;
	static final int OP_LD_L_D = 0x6A;
	static final int OP_LD_L_E = 0x6B;
	static final int OP_LD_L_H = 0x6C;
	static final int OP_LD_L_L = 0x6D;
	static final int OP_LD_L_HL_ADDR = 0x6E;

	static final int OP_LD_HL_ADDR_A = 0x77;
	static final int OP_LD_HL_ADDR_B = 0x70;
	static final int OP_LD_HL_ADDR_C = 0x71;
	static final int OP_LD_HL_ADDR_D = 0x72;
	static final int OP_LD_HL_ADDR_E = 0x73;
	static final int OP_LD_HL_ADDR_H = 0x74;
	static final int OP_LD_HL_ADDR_L = 0x75;

	static final int OP_LD_BC_ADDR_A = 0x02;
	static final int OP_LD_DE_ADDR_A = 0x12;
	static final int OP_LD_MEM_ADDR_A = 0x32;
	static final int OP_LD_A_BC_ADDR = 0x0A;
	static final int OP_LD_A_DE_ADDR = 0x1A;
	static final int OP_LD_A_MEM_ADDR = 0x3A;

	// 16-Bit Load Group
	static final int OP_LD_BC_VAL = 0x01;
	static final int OP_LD_DE_VAL = 0x11;
	static final int OP_LD_HL_VAL = 0x21;
	static final int OP_LD_SP_VAL = 0x31;

	static final int OP_PUSH_BC = 0xC5;
	static final int OP_PUSH_DE = 0xD5;
	static final int OP_PUSH_HL = 0xE5;
	static final int OP_PUSH_AF = 0xF5;

	static final int OP_POP_BC = 0xC1;
	static final int OP_POP_DE = 0xD1;
	static final int OP_POP_HL = 0xE1;
	static final int OP_POP_AF = 0xF1;

	// General-Purpose Arithmetic and CPU Control Group
	static final int OP_NOP = 0x00;
	static final int OP_DI  = 0xF3;
	static final int OP_EI  = 0xFB;
	static final int HALT   = 0x76;
	static final int OP_CCF = 0x3F;
	static final int OP_SCF = 0x37;
	static final int OP_CPL = 0x2F;
	static final int OP_DAA = 0x27;

	// 8-Bit Arithmetic Group
	static final int OP_ADD_A = 0x87;
	static final int OP_ADD_B = 0x80;
	static final int OP_ADD_C = 0x81;
	static final int OP_ADD_D = 0x82;
	static final int OP_ADD_E = 0x83;
	static final int OP_ADD_H = 0x84;
	static final int OP_ADD_L = 0x85;
	static final int OP_ADD_HL_ADDR = 0x86;

	static final int OP_ADC_A = 0x8F;
	static final int OP_ADC_B = 0x88;
	static final int OP_ADC_C = 0x89;
	static final int OP_ADC_D = 0x8A;
	static final int OP_ADC_E = 0x8B;
	static final int OP_ADC_H = 0x8C;
	static final int OP_ADC_L = 0x8D;
	static final int OP_ADC_HL_ADDR = 0x8E;

	static final int OP_SUB_A = 0x97;
	static final int OP_SUB_B = 0x90;
	static final int OP_SUB_C = 0x91;
	static final int OP_SUB_D = 0x92;
	static final int OP_SUB_E = 0x93;
	static final int OP_SUB_H = 0x94;
	static final int OP_SUB_L = 0x95;
	static final int OP_SUB_HL_ADDR = 0x96;
	static final int OP_SUB_VAL = 0xD6;

	static final int OP_SBC_A = 0x9F;
	static final int OP_SBC_B = 0x98;
	static final int OP_SBC_C = 0x99;
	static final int OP_SBC_D = 0x9A;
	static final int OP_SBC_E = 0x9B;
	static final int OP_SBC_H = 0x9C;
	static final int OP_SBC_L = 0x9D;
	static final int OP_SBC_HL_ADDR = 0x9E;

	static final int OP_AND_A = 0xA7;
	static final int OP_AND_B = 0xA0;
	static final int OP_AND_C = 0xA1;
	static final int OP_AND_D = 0xA2;
	static final int OP_AND_E = 0xA3;
	static final int OP_AND_H = 0xA4;
	static final int OP_AND_L = 0xA5;
	static final int OP_AND_HL_ADDR = 0xA6;

	static final int OP_XOR_A = 0xAF;
	static final int OP_XOR_B = 0xA8;
	static final int OP_XOR_C = 0xA9;
	static final int OP_XOR_D = 0xAA;
	static final int OP_XOR_E = 0xAB;
	static final int OP_XOR_H = 0xAC;
	static final int OP_XOR_L = 0xAD;
	static final int OP_XOR_HL_ADDR = 0xAE;
	static final int OP_XOR_VAL = 0xEE;

	static final int OP_OR_A = 0xB7;
	static final int OP_OR_B = 0xB0;
	static final int OP_OR_C = 0xB1;
	static final int OP_OR_D = 0xB2;
	static final int OP_OR_E = 0xB3;
	static final int OP_OR_H = 0xB4;
	static final int OP_OR_L = 0xB5;
	static final int OP_OR_HL_ADDR = 0xB6;

	static final int OP_CP_A = 0xBF;
	static final int OP_CP_B = 0xB8;
	static final int OP_CP_C = 0xB9;
	static final int OP_CP_D = 0xBA;
	static final int OP_CP_E = 0xBB;
	static final int OP_CP_H = 0xBC;
	static final int OP_CP_L = 0xBD;
	static final int OP_CP_HL_ADDR = 0xBE;
	static final int OP_CP_VAL = 0xFE;

	static final int OP_INC_A = 0x3C;
	static final int OP_INC_B = 0x04;
	static final int OP_INC_C = 0x0C;
	static final int OP_INC_D = 0x14;
	static final int OP_INC_E = 0x1C;
	static final int OP_INC_H = 0x24;
	static final int OP_INC_L = 0x2C;
	static final int OP_INC_HL_ADDR = 0x34;

	static final int OP_DEC_A = 0x3D;
	static final int OP_DEC_B = 0x05;
	static final int OP_DEC_C = 0x0D;
	static final int OP_DEC_D = 0x15;
	static final int OP_DEC_E = 0x1D;
	static final int OP_DEC_H = 0x25;
	static final int OP_DEC_L = 0x2D;
	static final int OP_DEC_HL_ADDR = 0x35;

	// 16-Bit Arithmetic Group
	static final int OP_INC_BC = 0x03;
	static final int OP_INC_DE = 0x13;
	static final int OP_INC_HL = 0x23;
	static final int OP_INC_SP = 0x33;
	static final int OP_DEC_BC = 0x0B;
	static final int OP_DEC_DE = 0x1B;
	static final int OP_DEC_HL = 0x2B;
	static final int OP_DEC_SP = 0x3B;

	// Rotate and Shift Group
	static final int OP_RLCA = 0x07;
	static final int OP_RRCA = 0x0F;
	static final int OP_RLA  = 0x017;
	static final int OP_RRA  = 0x01F;

	// Input and Ouput Group
	static final int OP_IN_A_ADDR = 0xDB;
	static final int OP_OUT_VAL_A = 0xD3;

	// Bit Set, Reset, and Test Group
	public static final int OP_BIT_REG = 0xCB;

	// Jump Group
	static final int OP_JP_NN = 0xC3;
	static final int OP_JP_NZ = 0xC2;
	static final int OP_JP_Z  = 0xCA;
	static final int OP_JP_NC = 0xD2;
	static final int OP_JP_C  = 0xDA;
	static final int OP_JP_PO = 0xE2;
	static final int OP_JP_PE = 0xEA;
	static final int OP_JP_P  = 0xF2;
	static final int OP_JP_M  = 0xFA;
	static final int OP_DJNZ  = 0x10;
	static final int OP_JR_E  = 0x18;
	static final int OP_JR_NZ = 0x20;
	static final int OP_JR_Z  = 0x28;
	static final int OP_JR_NC = 0x30;
	static final int OP_JR_C  = 0x38;

	// Call and Return Group
	static final int OP_RET      = 0xC9;
	static final int OP_CALL_VAL = 0xCD;

	static final int ED_IM0  = 0x46;
	static final int ED_IM0A = 0x4E;
	static final int ED_IM1  = 0x56;
	static final int ED_IM0B = 0x66;
	static final int ED_IM2  = 0x5E;
	static final int ED_IM0C = 0x6E;
	static final int ED_IM1A = 0x76;
	static final int ED_IM2A = 0x7E;

	static final int ED_RETN  = 0x45;
	static final int ED_RETI  = 0x4D;
	static final int ED_RETNA = 0x55;
	static final int ED_RETNB = 0x5D;
	static final int ED_RETNC = 0x65;
	static final int ED_RETND = 0x6D;
	static final int ED_RETNE = 0x75;
	static final int ED_RETNF = 0x7D;

	static final int ED_OTIR = 0xB3;

	// Exchange, Block Transfer, and Search Group
	static final int ED_LDIR = 0xB0;

	public static final int OP_EXTD  = 0xED;
	public static final int OP_IX  = 0xDD;
	public static final int OP_IY  = 0xFD;

	// Reset
	static final int OP_RST_00 = 0xC7;
	static final int OP_RST_08 = 0xCF;
	static final int OP_RST_10 = 0xD7;
	static final int OP_RST_18 = 0xDF;
	static final int OP_RST_20 = 0xE7;
	static final int OP_RST_28 = 0xEF;
	static final int OP_RST_30 = 0xF7;
	static final int OP_RST_38 = 0xFF;

	public Z80(final MemoryChip mem) {
		this.mem = mem;

		reset();
	}

	public void emulate() {
		while (true) {
			doCycle();
		}
	}

	@Override
	public void reset() {
		AF.set(0xFFFF);
		SP.set(0xFF);
		PC.set(0x00);
		IFF1 = IFF2 = false;
		IM = 0;
	}

	@Override
	public void doCycle() {
		int op = fetch();

		decodeOp(op);

//		if (counter <= 0) {
//			/* Check interruptions */
//			counter += interruptPeriod;
//			if (exitRequired) {
//				throw new RuntimeException();
//			}
//		}
	}

	private int fetch() {
		int op = mem.readUnsigned(PC.inc());

		// Add cycles
		counter -= 4;
		clock += 4;

		return op;
	}

	private int readMem8() {
		counter -= 3;
		clock += 3;

		return mem.readSigned(PC.inc());
	}

	private int readUnsignedMem8() {
		counter -= 3;
		clock += 3;

		return mem.readUnsigned(PC.inc());
	}

	private int readUnsignedMem16() {
		counter -= 6;
		clock += 6;

		return (mem.readUnsigned(PC.inc()) | mem.readUnsigned(PC.inc()) << 8);
	}

	private void decodeOp(int op) {
		int op2 = 0;
		// Debug vars
		int data = 0, oldPC = PC.get();
		switch (op) {
			case OP_NOP:
				break;

			case OP_LD_A_VAL:
				data = readUnsignedMem8();	AF.setH(data);	break;
			case OP_LD_B_VAL:
				data = readUnsignedMem8();	BC.setH(data);	break;
			case OP_LD_C_VAL:
				data = readUnsignedMem8();	BC.setL(data);	break;
			case OP_LD_D_VAL:
				data = readUnsignedMem8();	DE.setH(data);	break;
			case OP_LD_E_VAL:
				data = readUnsignedMem8();	DE.setL(data);	break;
			case OP_LD_H_VAL:
				data = readUnsignedMem8();	HL.setH(data);	break;
			case OP_LD_L_VAL:
				data = readUnsignedMem8();	HL.setL(data);	break;

			case OP_LD_A_A:
					AF.setH(AF.getH());	break;
			case OP_LD_A_B:
					AF.setH(BC.getH());	break;
			case OP_LD_A_C:
					AF.setH(BC.getL());	break;
			case OP_LD_A_D:
					AF.setH(DE.getH());	break;
			case OP_LD_A_E:
					AF.setH(DE.getL());	break;
			case OP_LD_A_H:
					AF.setH(HL.getH());	break;
			case OP_LD_A_L:
					AF.setH(HL.getL());	break;
			case OP_LD_A_HL_ADDR:
					// FIXME!
				break;

			case OP_LD_B_A:
					BC.setH(AF.getH());	break;
			case OP_LD_B_B:
					BC.setH(BC.getH());	break;
			case OP_LD_B_C:
					BC.setH(BC.getL());	break;
			case OP_LD_B_D:
					BC.setH(DE.getH());	break;
			case OP_LD_B_E:
					BC.setH(DE.getL());	break;
			case OP_LD_B_H:
					BC.setH(HL.getH());	break;
			case OP_LD_B_L:
					BC.setH(HL.getL());	break;
			case OP_LD_B_HL_ADDR:
					// FIXME!
				break;

			case OP_LD_C_A:
					BC.setL(AF.getH());	break;
			case OP_LD_C_B:
					BC.setL(BC.getH());	break;
			case OP_LD_C_C:
					BC.setL(BC.getL());	break;
			case OP_LD_C_D:
					BC.setL(DE.getH());	break;
			case OP_LD_C_E:
					BC.setL(DE.getL());	break;
			case OP_LD_C_H:
					BC.setL(HL.getH());	break;
			case OP_LD_C_L:
					BC.setL(HL.getL());	break;
			case OP_LD_C_HL_ADDR:
					// FIXME!
				break;

			case OP_LD_D_A:
					DE.setH(AF.getH());	break;
			case OP_LD_D_B:
					DE.setH(BC.getH());	break;
			case OP_LD_D_C:
					DE.setH(BC.getL());	break;
			case OP_LD_D_D:
					DE.setH(DE.getH());	break;
			case OP_LD_D_E:
					DE.setH(DE.getL());	break;
			case OP_LD_D_H:
					DE.setH(HL.getH());	break;
			case OP_LD_D_L:
					DE.setH(HL.getL());	break;
			case OP_LD_D_HL_ADDR:
					// FIXME!
				break;

			case OP_LD_E_A:
					DE.setL(AF.getH());	break;
			case OP_LD_E_B:
					DE.setL(BC.getH());	break;
			case OP_LD_E_C:
					DE.setL(BC.getL());	break;
			case OP_LD_E_D:
					DE.setL(DE.getH());	break;
			case OP_LD_E_E:
					DE.setL(DE.getL());	break;
			case OP_LD_E_H:
					DE.setL(HL.getH());	break;
			case OP_LD_E_L:
					DE.setL(HL.getL());	break;
			case OP_LD_E_HL_ADDR:
					// FIXME!
				break;

			case OP_LD_H_A:
					HL.setH(AF.getH());	break;
			case OP_LD_H_B:
					HL.setH(BC.getH());	break;
			case OP_LD_H_C:
					HL.setH(BC.getL());	break;
			case OP_LD_H_D:
					HL.setH(DE.getH());	break;
			case OP_LD_H_E:
					HL.setH(DE.getL());	break;
			case OP_LD_H_H:
					HL.setH(HL.getH());	break;
			case OP_LD_H_L:
					HL.setH(HL.getL());	break;
			case OP_LD_H_HL_ADDR:
					// FIXME!
				break;

			case OP_LD_L_A:
					HL.setL(AF.getH());	break;
			case OP_LD_L_B:
					HL.setL(BC.getH());	break;
			case OP_LD_L_C:
					HL.setL(BC.getL());	break;
			case OP_LD_L_D:
					HL.setL(DE.getH());	break;
			case OP_LD_L_E:
					HL.setL(DE.getL());	break;
			case OP_LD_L_H:
					HL.setL(HL.getH());	break;
			case OP_LD_L_L:
					HL.setL(HL.getL());	break;
			case OP_LD_L_HL_ADDR:
					// FIXME!
				break;

			/*case OP_LD_HL_ADDR_A:
					HL.setHL(ADDR_A.getHL());	break;
			case OP_LD_HL_ADDR_B:
					HL.setHL(ADDR_B.getHL());	break;
			case OP_LD_HL_ADDR_C:
					HL.setHL(ADDR_C.getHL());	break;
			case OP_LD_HL_ADDR_D:
					HL.setHL(ADDR_D.getHL());	break;
			case OP_LD_HL_ADDR_E:
					HL.setHL(ADDR_E.getHL());	break;
			case OP_LD_HL_ADDR_H:
					HL.setHL(ADDR_H.getHL());	break;
			case OP_LD_HL_ADDR_L:
					HL.setHL(ADDR_L.getHL());	break;*/

			case OP_LD_HL_ADDR:
				data = readUnsignedMem8();
				// FIXME!
				break;

			case OP_LD_BC_VAL:
					data = readUnsignedMem16();	BC.set(data);	break;
			case OP_LD_DE_VAL:
					data = readUnsignedMem16();	DE.set(data);	break;
			case OP_LD_HL_VAL:
					data = readUnsignedMem16();	HL.set(data);	break;
			case OP_LD_SP_VAL:
					data = readUnsignedMem16();	SP.set(data);	break;	// FIXME?

			case OP_XOR_A:
					AF.setH(alu.xor(AF.getH(), AF.getH()));		break;
			case OP_XOR_B:
					AF.setH(alu.xor(AF.getH(), BC.getH()));		break;
			case OP_XOR_C:
					AF.setH(alu.xor(AF.getH(), BC.getL()));		break;
			case OP_XOR_D:
					AF.setH(alu.xor(AF.getH(), DE.getH()));		break;
			case OP_XOR_E:
					AF.setH(alu.xor(AF.getH(), DE.getL()));		break;
			case OP_XOR_H:
					AF.setH(alu.xor(AF.getH(), HL.getH()));		break;
			case OP_XOR_L:
					AF.setH(alu.xor(AF.getH(), HL.getL()));		break;

			case OP_OR_A:
					AF.setH(alu.or(AF.getH(), AF.getH()));		break;
			case OP_OR_B:
					AF.setH(alu.or(AF.getH(), BC.getH()));		break;
			case OP_OR_C:
					AF.setH(alu.or(AF.getH(), BC.getL()));		break;
			case OP_OR_D:
					AF.setH(alu.or(AF.getH(), DE.getH()));		break;
			case OP_OR_E:
					AF.setH(alu.or(AF.getH(), DE.getL()));		break;
			case OP_OR_H:
					AF.setH(alu.or(AF.getH(), HL.getH()));		break;
			case OP_OR_L:
					AF.setH(alu.or(AF.getH(), HL.getL()));		break;

			case OP_DEC_A:
					AF.decH();	break;
			case OP_DEC_B:
					BC.decH();	break;
			case OP_DEC_C:
					BC.decL();	break;
			case OP_DEC_D:
					DE.decH();	break;
			case OP_DEC_E:
					DE.decL();	break;
			case OP_DEC_H:
					HL.decH();	break;
			case OP_DEC_L:
					HL.decL();	break;

			case OP_DEC_BC:
					BC.dec();	break;
			case OP_DEC_DE:
					DE.dec();	break;
			case OP_DEC_HL:
					HL.dec();	break;
			case OP_DEC_SP:
					SP.dec();	break;	// FIXME

			case OP_IN_A_ADDR:
				data = readMem8();	// FIXME
				break;
			case OP_OUT_VAL_A:
				data = readMem8();	// FIXME
				break;

			case OP_RET:
				PC.set((mem.readUnsigned(SP) << 8) | mem.readUnsigned(SP.get() + 1));
				SP.set(SP.get() + 2);
				break;
			case OP_CALL_VAL:
				data = readUnsignedMem16();
				mem.write(SP.get(),     (byte)((PC.get() & 0xFF00) >> 8));
				mem.write(SP.get() - 1, (byte)(PC.get() & 0x00FF));
				SP.set(SP.get() - 2);
				PC.set(data);
				break;

			case OP_DI:
				IFF1 = IFF2 = false;	break;
			case OP_EI:
				IFF1 = IFF2 = true;		break;
			case OP_BIT_REG: {
				data = readMem8();
				int bit = (data >> 3) & 0x07;
				int bitOp = data >> 6;
				int reg = data & 0x07;
				switch (bitOp) {
					case 1:
						// BIT b, r | BIT b, (HL)
						// FIXME
						break;
					case 2:
						// RES b, m | RES b, (HL)
						// FIXME
						break;
					case 3:
						// SET b, r | SET b, (HL)
						// FIXME
						break;
				}
				break;
			}
			case OP_JP_NN:
				data = readUnsignedMem16();	PC.set(data);	break;
			case OP_JR_E:
				data = readMem8();
				PC.set(PC.get() + (byte)data);
				break;
			case OP_JR_NZ:
				data = readMem8();
				PC.set(PC.get() + (F.get(Z80Flags.FLAG_ZERO) ? 0 : (byte)data));
				break;
			case OP_DJNZ:
				data = readMem8();
				BC.decH();
				PC.set(PC.get() + (F.get(Z80Flags.FLAG_ZERO) ? 0 : (byte)data));
				break;
			case OP_RST_00:
			case OP_RST_08:
			case OP_RST_10:
			case OP_RST_18:
			case OP_RST_20:
			case OP_RST_28:
			case OP_RST_30:
			case OP_RST_38:
				// FIXME
				break;
			case OP_EXTD:
				op2 = fetch();
				switch (op2) {
					case ED_IM0:
					case ED_IM0A:
					case ED_IM0B:
					case ED_IM0C:
						IM = 0;		break;
					case ED_IM1:
					case ED_IM1A:
						IM = 1;		break;
					case ED_IM2:
					case ED_IM2A:
						IM = 2;		break;
					case ED_LDIR:
						// FIXME!
						break;
					case ED_OTIR:
						// FIXME!
						break;
					default:
						throw new UnknownOpcodeException(op, op2, PC);
				}
				break;
			default:
				throw new UnknownOpcodeException(op, PC);
		}
	}

	/**
	 * For debugging purposes
	 */
	@Override
	public MemoryChip getMemory() {
		return mem;
	}

	public Z80Register getRegAF() {
		return AF;
	}

	public Z80Register getRegBC() {
		return BC;
	}

	public Z80Register getRegDE() {
		return DE;
	}

	public Z80Register getRegHL() {
		return HL;
	}

	@Override
	public long getClockCycles() {
		return clock;
	}

	@Override
	public long getNmiAddr() {
		return 0x0066;
	}

	@Override
	public long getPorAddr() {
		return 0x0000;
	}

	@Override
	public long[] getIntAddrs() {
		return new long[] {0x08, 0x10, 0x18, 0x20, 0x28, 0x30, 0x38};
	}

	@Override
	public Register getPc() {
		return PC;
	}

	@Override
	public Register getSp() {
		return SP;
	}

	@Override
	public Flags getFlags() {
		return F;
	}

	@Override
	public int getNumRegisters() {
		return 4;
	}

	@Override
	public Register getRegister(int idx) {
		switch (idx) {
			case 0: return AF;
			case 1: return BC;
			case 2: return DE;
			case 3: return HL;
			default: return null;
		}
	}

	@Override
	public int getNumCounters() {
		return 4;
	}

	@Override
	public Register getCounter(int idx) {
		switch (idx) {
			case DEBUG_IDX_PC: return PC;
			case DEBUG_IDX_SP: return SP;
			case DEBUG_IDX_IX: return IX;
			case DEBUG_IDX_IY: return IY;
			default: throw new RuntimeException("Invalid counter");
		}
	}
}
