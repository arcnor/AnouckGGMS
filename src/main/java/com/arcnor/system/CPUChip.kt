package com.arcnor.system

import com.arcnor.cpu.Flags
import com.arcnor.cpu.Register

interface CPUChip {
	/**
	 * Performs a reset of the processor. Registers, PC and any peripheral should be set at initial values
	 */
	fun reset()

	/**
	 * Process a full cycle (fetch, decode and execute)
	 */
	fun doCycle()

	/**
	 * The memory wired to this CPU.
	 */
	val memory: MemoryChip

	/**
	 * The current clock cycles
	 */
	val clockCycles: Long

	/**
	 * The address of the Non Maskable Interrupt on memory
	 */
	val nmiAddr: Long

	/**
	 * The address of Power On Reset location on memory
	 */
	val porAddr: Long

	/**
	 * The address(es) of the Interrupt handler(s) on memory
	 */
	val intAddrs: LongArray

	/**
	 * The Program Counter
	 */
	val pc: Register

	/**
	 * The Stack Pointer
	 */
	val sp: Register

	/**
	 * The Flag register
	 */
	val flags: Flags

	/**
	 * The number of registers this processor has
	 */
	val numRegisters: Int

	/**
	 * Retrieves the register with index idx. Registers from 0 to [.getNumRegisters] should exist
	 */
	fun getRegister(idx: Int): Register

	/**
	 * The number of counters (like PC and SP)
	 */
	val numCounters: Int

	/**
	 * The value of the counter with index idx. Counters from 0 to [.getNumCounters] should exist
	 */
	fun getCounter(idx: Int): Register
}
