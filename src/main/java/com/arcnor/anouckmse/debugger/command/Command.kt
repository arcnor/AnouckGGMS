package com.arcnor.anouckmse.debugger.command

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog
import javax.swing.Action

abstract class Command(protected val console: ConsoleDialog, val name: String, val desc: String, val action: Action) {
	open val isStepCommand
		get() = false

	open fun onStep() {
		// Do nothing
	}
}
