package com.arcnor.anouckmse.machine.c64;

import com.arcnor.cpu.debugger.MachineDebug;

public class C64Debug implements MachineDebug {
	@Override
	public String formatLabel(int addr) {
		switch (addr) {
			case 0xFD02: return "CHKCART";
			case 0xFD15: return "RESTOR";
			case 0xFDA3: return "IOINIT";
			case 0xFD50: return "RAMTAS";
			case 0xFF5B: return "CINT";
		}
		return null;
	}

	private int getMaskedAddr(int addr) {
		int region = (addr & 0xFF00) >> 8;
		int newAddr = addr;
		if (region < 0xD4) {
			newAddr = addr & 0xD03F;
		} else if (region < 0xD8) {
			newAddr = addr & 0xD43F;
		} else if (region < 0xDC) {
		} else if (region < 0xDD) {
			newAddr = addr & 0xDC0F;
		} else if (region < 0xDE) {
			newAddr = addr & 0xDD0F;
		}
		return newAddr;
	}

	@Override
	public String formatReadAddr(int addr) {
		return formatAddr(getMaskedAddr(addr));
	}

	@Override
	public String formatWriteAddr(int addr) {
		return formatAddr(getMaskedAddr(addr));
	}

	private String formatAddr(final int addr) {
		switch (addr) {
			// Registers
			case 0xD000: return "SPR0X";
			case 0xD001: return "SPR0Y";
			case 0xD002: return "SPR1X";
			case 0xD003: return "SPR1Y";
			case 0xD004: return "SPR2X";
			case 0xD005: return "SPR2Y";
			case 0xD006: return "SPR3X";
			case 0xD007: return "SPR3Y";
			case 0xD008: return "SPR4X";
			case 0xD009: return "SPR4Y";
			case 0xD00A: return "SPR5X";
			case 0xD00B: return "SPR5Y";
			case 0xD00C: return "SPR6X";
			case 0xD00D: return "SPR6Y";
			case 0xD00E: return "SPR7X";
			case 0xD00F: return "SPR7Y";
			case 0xD010: return "SPR0-7X";
			case 0xD011: return "SCRREG1";
			case 0xD012: return "RASTER";
			case 0xD013: return "LIGHTX";
			case 0xD014: return "LIGHTY";
			case 0xD015: return "SPRENABLE";
			case 0xD016: return "SCRREG2";
			case 0xD017: return "SPR2HEIGHT";
			case 0xD018: return "MEMSETUP";
			case 0xD019: return "INTSTATUS";
			case 0xD01A: return "INTREG";
			case 0xD01B: return "SPRPRIO";
			case 0xD01C: return "SPRMUCOLORSPR";
			case 0xD01D: return "SPRDBLWREG";
			case 0xD01E: return "SPRSPRCOLREG";
			case 0xD01F: return "SPRBGCOLLREG";
			case 0xD020: return "BORDERCOLOR";
			case 0xD021: return "BGCOLOR";
			case 0xD022: return "EXTRABGCOLOR1";
			case 0xD023: return "EXTRABGCOLOR2";
			case 0xD024: return "EXTRABGCOLOR3";
			case 0xD025: return "EXTRASPRCOLOR1";
			case 0xD026: return "EXTRASPRCOLOR2";
			case 0xD027: return "SPRCOLOR0";
			case 0xD028: return "SPRCOLOR1";
			case 0xD029: return "SPRCOLOR2";
			case 0xD02A: return "SPRCOLOR3";
			case 0xD02B: return "SPRCOLOR4";
			case 0xD02C: return "SPRCOLOR5";
			case 0xD02D: return "SPRCOLOR6";
			case 0xD02E: return "SPRCOLOR7";

			// SID
			case 0xD400: return "VOICE1FREQ";       // Write only from here
			case 0xD402: return "VOICE1PULSEW";
			case 0xD404: return "VOICE1CTRLREG";
			case 0xD405: return "VOICE1ATKDCAY";
			case 0xD406: return "VOICE1SUSTREL";
			case 0xD407: return "VOICE2FREQ";
			case 0xD409: return "VOICE2PULSEW";
			case 0xD40B: return "VOICE2CTRLREG";
			case 0xD40C: return "VOICE2ATKDCAY";
			case 0xD40D: return "VOICE2SUSTREL";
			case 0xD40E: return "VOICE3FREQ";
			case 0xD410: return "VOICE3PULSEW";
			case 0xD412: return "VOICE3CTRLREG";
			case 0xD413: return "VOICE3ATKDCAY";
			case 0xD414: return "VOICE3SUSTREL";
			case 0xD415: return "FILTERCUTOFF1";
			case 0xD416: return "FILTERCUTOFF2";
			case 0xD417: return "FILTERCTRL";
			case 0xD418: return "VOLFILTERMODE";
			case 0xD419: return "PADDLEX";    // Read only from here
			case 0xD41A: return "PADDLEY";
			case 0xD41B: return "VOICE3WAVEFORM";
			case 0xD41C: return "VOICE3ADSR";

			// CIA#1: Inputs (keyboard, joystick, mouse), datasette, IRQ control
			case 0xDC00: return "PORTAKBDJOY_1";
			case 0xDC01: return "PORTBKBDJOY_1";
			case 0xDC02: return "PORTADATADIR_1";
			case 0xDC03: return "PORTBDATADIR_1";
			case 0xDC04: return "TIMERA_1";
			case 0xDC06: return "TIMERB_1";
			case 0xDC08: return "TIMEMS_1";
			case 0xDC09: return "TIMESEC_1";
			case 0xDC0A: return "TIMEMIN_1";
			case 0xDC0B: return "TIMEHOUR_1";
			case 0xDC0C: return "SERIALSHIFTREG_1";
			case 0xDC0D: return "INTCTRLREG_1";
			case 0xDC0E: return "TIMERACTRLREG_1";
			case 0xDC0F: return "TIMERBCTRLREG_1";

			// CIA#2: Serial bus, RS232, NMI control
			case 0xDD00: return "PORTASERIALBUS_2";
			case 0xDD01: return "PORTBRS232_2";
			case 0xDD02: return "PORTADATADIR_2";
			case 0xDD03: return "PORTBDATADIR_2";
			case 0xDD04: return "TIMERA_2";
			case 0xDD06: return "TIMERB_2";
			case 0xDD08: return "TIMEMS_2";
			case 0xDD09: return "TIMESEC_2";
			case 0xDD0A: return "TIMEMIN_2";
			case 0xDD0B: return "TIMEHOUR_2";
			case 0xDD0C: return "SERIALSHIFTREG_2";
			case 0xDD0D: return "INTCTRLREG_2";
			case 0xDD0E: return "TIMERACTRLREG_2";
			case 0xDD0F: return "TIMERBCTRLREG_2";
		}
		return null;
	}

	@Override
	public String commentReadAddr(int addr) {
		switch (addr) {
			case 0xD000: return "";
			case 0xD001: return "";
			case 0xD002: return "";
			case 0xD003: return "";
			case 0xD004: return "";
			case 0xD005: return "";
			case 0xD006: return "";
			case 0xD007: return "";
			case 0xD008: return "";
			case 0xD009: return "";
			case 0xD00A: return "";
			case 0xD00B: return "";
			case 0xD00C: return "";
			case 0xD00D: return "";
			case 0xD00E: return "";
			case 0xD00F: return "";
			case 0xD010: return "";
			case 0xD011: return "";
			case 0xD012: return "";
			case 0xD013: return "";
			case 0xD014: return "";
			case 0xD015: return "";
			case 0xD016: return "";
			case 0xD017: return "";
			case 0xD018: return "";
			case 0xD019: return "";
			case 0xD01A: return "";
			case 0xD01B: return "";
			case 0xD01C: return "";
			case 0xD01D: return "";
			case 0xD01E: return "";
			case 0xD01F: return "";
			case 0xD020: return "";
			case 0xD021: return "";
			case 0xD022: return "";
			case 0xD023: return "";
			case 0xD024: return "";
			case 0xD025: return "";
			case 0xD026: return "";
			case 0xD027: return "";
			case 0xD028: return "";
			case 0xD029: return "";
			case 0xD02A: return "";
			case 0xD02B: return "";
			case 0xD02C: return "";
			case 0xD02D: return "";
			case 0xD02E: return "";
		}
		return null;  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public String commentWriteAddr(int addr) {
		return null;  //To change body of implemented methods use File | Settings | File Templates.
	}
}
