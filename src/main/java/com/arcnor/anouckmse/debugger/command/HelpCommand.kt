package com.arcnor.anouckmse.debugger.command

import com.arcnor.anouckmse.debugger.dialog.ConsoleDialog

import java.awt.event.ActionEvent
import javax.swing.AbstractAction

class HelpCommand(console: ConsoleDialog) : Command(console, "?", "Provides a list of commands", object : AbstractAction("Help Command") {
	override fun actionPerformed(e: ActionEvent) {
		console.append("Currently known commands are:\n", ConsoleDialog.STYLE_INFO)
		for (cmd in console.getCommands()) {
			console.append("   " + cmd.name + "\t" + cmd.desc + "\n", ConsoleDialog.STYLE_INFO)
		}
	}
})
