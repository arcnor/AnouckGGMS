package com.arcnor.cpu.intel8080

import com.arcnor.cpu.Flags
import com.arcnor.cpu.Register16
import com.arcnor.cpu.Register8
import com.arcnor.cpu.z80.Intel8080Flags
import com.arcnor.system.CPUChip
import com.arcnor.system.MemoryChip

open class Intel8080(override val memory: MemoryChip) : CPUChip {
	override fun reset() {

	}

	override fun doCycle() {

	}

	override val clockCycles: Long = 0
	override val nmiAddr = 0L
	override val porAddr = 0L
	override val intAddrs = longArrayOf()
	override val pc = Register16("PC")
	override val sp = Register16("SP")
	override val flags: Flags = Intel8080Flags()
	override val numRegisters = 4

	val regA = Register8("A")
	val regBC = Register16("BC")
	val regDE = Register16("DE")
	val regHL = Register16("HL")

	override fun getRegister(idx: Int) = when(idx) {
		0 -> regA
		1 -> regBC
		2 -> regDE
		3 -> regHL
		else -> throw RuntimeException("Invalid register")
	}

	override val numCounters = 2

	override fun getCounter(idx: Int) = when (idx) {
		0 -> pc
		1 -> sp
		else -> throw RuntimeException("Invalid counter")
	}
}