package com.arcnor.anouckmse.swing

import javax.swing.*

interface MutableListModel<T> : ListModel<T> {
	fun add(obj: T): Boolean
	operator fun contains(obj: T): Boolean
	fun remove(obj: T): Boolean
	fun remove(index: Int): Boolean
}
