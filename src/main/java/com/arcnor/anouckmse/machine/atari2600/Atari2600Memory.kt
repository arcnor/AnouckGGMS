package com.arcnor.anouckmse.machine.atari2600

import com.arcnor.cpu.Register
import com.arcnor.system.MemoryChip
import java.util.Arrays

@Suppress("OVERRIDE_BY_INLINE", "NOTHING_TO_INLINE")
class Atari2600Memory(mem: ByteArray) : MemoryChip {
	private val mem: ByteArray

	init {
		if (mem.size != 0x1000) {
			throw RuntimeException("Unsupported memory size (" + mem.size + ")")
		}
		this.mem = ByteArray(0x10000)
		System.arraycopy(mem, 0, this.mem, 0xF000, mem.size)
	}

	override fun getTotalSize() = mem.size

	override fun readSigned(addr: Int) = mem[addr]

	override fun readUnsigned(addr: Int) = mem[addr].toInt() and 0xFF
	override inline fun readSigned(addr: Register) = readSigned(addr.get())
	override inline fun readUnsigned(addr: Register) = readUnsigned(addr.get())

	override fun write(addr: Int, value: Byte) {
		mem[addr] = value
	}

	override fun getData(addr: Int, length: Int) = Arrays.copyOfRange(mem, addr, addr + length)

	override val regionNames = arrayOf("0 PAGE", "STACK", "RAM", "IO", "ROM", "VECTOR")

	override fun getRegionLength(region: Int) = when (region) {
		0 -> 0x0100L
		1 -> 0x0100L
		2 -> 0x3E00L
		3 -> 0x4000L
		4 -> 0x7FF9L
		5 -> 0x0006L
		else -> 0
	}

	override fun getRegionOffset(region: Int, offset: Long) = when (region) {
		0 -> 0x0000L
		1 -> 0x0100L
		2 -> 0x0200L
		3 -> 0x4000L
		4 -> 0x8000L
		5 -> 0xFFFAL
		else -> 0L
	}
}
