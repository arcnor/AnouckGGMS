package com.arcnor.cpu

interface Register {
	val name: String
	/**
	 * Size, in bytes
	 */
	val size: Int

	fun get(): Int
	fun set(value: Int)

	operator fun inc(): Register
	operator fun dec(): Register
}

abstract class AbstractRegister(override val size: Int) : Register {
	protected var value: Int = 0

	private val MAX = Math.pow(2.0, size * 8.0).toInt() - 1

	override fun get() = value

	override fun set(value: Int) {
		this.value = value and MAX
	}

	override fun inc(): Register {
		this.value.inc()
		if (this.value > MAX) {
			this.value = 0
		}
		return this
	}

	override fun dec(): Register {
		this.value.dec()
		if (this.value < 0) {
			this.value = MAX
		}
		return this
	}

	override fun toString() = String.format("0x%1$0${MAX}X (%1\$d)", value)
}

open class Register8(override val name: String) : AbstractRegister(1)

open class Register16(override val name: String) : AbstractRegister(2) {
	var l: Int
		get() = value and 0xFF
		set(newValue) {
			value = (value and 0xFF00) or (newValue and 0xFF)
		}
	var h: Int
		get() = (value and 0xFF00) ushr 8
		set(newValue) {
			value = (value and 0x00FF) or ((newValue and 0xFF) shl 8)
		}

	override fun toString() = String.format("0x%1$02X (%1\$d) - 0x%2$02X (%2\$d)", h, l)
}
