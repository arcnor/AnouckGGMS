package com.arcnor.anouckmse.debugger.dialog

import com.arcnor.anouckmse.debugger.ASMTableModel
import com.arcnor.anouckmse.debugger.Debugger
import com.arcnor.anouckmse.swing.MutableListModel
import java.awt.BorderLayout
import java.awt.event.ActionEvent
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.AbstractAction
import javax.swing.Action
import javax.swing.JList
import javax.swing.JScrollPane
import javax.swing.KeyStroke

class BreakpointsDialog(owner: Debugger, private val breakpointListModel: MutableListModel<ASMTableModel.BreakpointPosition>) : DebugDialog(owner, "Breakpoint list", false) {
	private val actDelete: Action
	private val actGoLine: Action

	init {
		actDelete = object : AbstractAction("Delete breakpoint") {
			override fun actionPerformed(e: ActionEvent) {
				val list = e.source as JList<*>
				val selIdx = list.selectedIndex
				if (selIdx >= 0) {
					this@BreakpointsDialog.breakpointListModel.remove(selIdx)
				}
			}
		}
		actGoLine = object : AbstractAction("Goto breakpoint line") {
			override fun actionPerformed(e: ActionEvent) {
				val list = e.source as JList<*>
				val selIdx = list.selectedIndex
				if (selIdx >= 0) {
					owner.gotoLine(this@BreakpointsDialog.breakpointListModel.getElementAt(selIdx).position)
				}
			}
		}

		layout = BorderLayout()
		val list = JList(breakpointListModel)
		list.background = owner.background
		list.foreground = owner.foreground
		list.selectionBackground = Debugger.COLOR_BACK_SELECTION
		list.selectionForeground = owner.foreground
		list.inputMap.put(KeyStroke.getKeyStroke("DELETE"), "delete")
		list.actionMap.put("delete", actDelete)
		list.addMouseListener(object : MouseAdapter() {
			override fun mouseClicked(e: MouseEvent?) {
				if (e!!.clickCount == 2) {
					val idx = (e.source as JList<*>).locationToIndex(e.point)
					if (idx >= 0) {
						actGoLine.actionPerformed(ActionEvent(e.source, ActionEvent.ACTION_PERFORMED, "dblClick"))
					}
				}
			}
		})
		add(JScrollPane(list))

		setSize(100, owner.height)
		val ownerLoc = owner.location
		setLocation(ownerLoc.getX().toInt() + owner.width, ownerLoc.getY().toInt())
	}

	override fun onStep() {
		// Do nothing
	}

	override fun onGraphicalStep() {
		// Do nothing
	}
}
