package com.arcnor.system

import java.awt.Graphics2D
import java.awt.GraphicsEnvironment
import java.awt.Transparency
import java.awt.image.BufferedImage

abstract class VideoChip(val width: Int, val height: Int) {
	val videoSurface: BufferedImage
	protected val g2: Graphics2D

	init {
		val ge = GraphicsEnvironment.getLocalGraphicsEnvironment()
		val gs = ge.defaultScreenDevice
		val gc = gs.defaultConfiguration

		videoSurface = gc.createCompatibleImage(width, height, Transparency.OPAQUE)
		g2 = videoSurface.createGraphics()
	}
}
