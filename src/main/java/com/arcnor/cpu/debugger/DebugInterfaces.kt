package com.arcnor.cpu.debugger

import com.arcnor.system.CPUChip

interface MachineDebug : MemoryDebug
interface ROMDebug : MemoryDebug

interface MemoryDebug {
	fun formatLabel(addr: Int): String?
	fun formatReadAddr(addr: Int): String?
	fun formatWriteAddr(addr: Int): String?
	fun commentReadAddr(addr: Int): String?
	fun commentWriteAddr(addr: Int): String?
}

interface CPUDebug<in T : CPUChip> {
	fun decodeOpcodeInfo(opcode: Int): OpcodeInfo
	fun opcodeJumpType(opcode: Int, data: Int): JumpType
	fun opcodeJumpData(opcode: Int, data: Int): Int
	fun opToStr(oldPC: Int, opcode: Int, op: Int, op2: Int, data: Int, cpu: T): String
	fun formatLabel(addr: Int): String
}