package com.arcnor.cpu.mos6502

import com.arcnor.cpu.Flags

class MOS6502Flags : Flags() {
	companion object {
		private val FLAG_NAMES = arrayOf(
				"C",
				"Z",
				"I",
				"D",
				"B",
				"-",
				"V",
				"N"
		)

		@JvmField
		val FLAG_CARRY = 0
		@JvmField
		val FLAG_ZERO = 1
		@JvmField
		val FLAG_INTERRUPT = 2
		@JvmField
		val FLAG_DECIMAL = 3
		@JvmField
		val FLAG_BREAK = 4
		@JvmField
		val FLAG_UNUSED = 5
		@JvmField
		val FLAG_OVERFLOW = 6
		@JvmField
		val FLAG_NEGATIVE = 7
	}

	override fun getName(index: Int) = FLAG_NAMES[index]

	override fun reset() {
		super.reset()
		set(FLAG_INTERRUPT, true)
		set(FLAG_UNUSED, true)
	}

	override fun get(): Int {
		return (if (flag[7]) 1 else 0) shl 7 or (
				(if (flag[6]) 1 else 0) shl 6) or
				0x20 or (
				(if (flag[4]) 1 else 0) shl 4) or (
				(if (flag[3]) 1 else 0) shl 3) or (
				(if (flag[2]) 1 else 0) shl 2) or (
				(if (flag[1]) 1 else 0) shl 1) or
				if (flag[0]) 1 else 0
	}

	override fun set(value: Int) {
		flag[7] = value and 0x80 != 0
		flag[6] = value and 0x40 != 0
		flag[4] = value and 0x10 != 0
		flag[3] = value and 0x08 != 0
		flag[2] = value and 0x04 != 0
		flag[1] = value and 0x02 != 0
		flag[0] = value and 0x01 != 0
	}
}
