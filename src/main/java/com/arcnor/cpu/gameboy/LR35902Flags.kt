package com.arcnor.cpu.gameboy

import com.arcnor.cpu.Flags

class LR35902Flags : Flags() {
	companion object {
		private val FLAG_NAMES = arrayOf(
				"-",
				"-",
				"-",
				"-",
				"C",
				"H",
				"N",
				"Z"
		)

		@JvmField
		val FLAG_CARRY = 4
		@JvmField
		val FLAG_HALFCARRY = 5
		@JvmField
		val FLAG_NEG = 6
		@JvmField
		val FLAG_ZERO = 7
	}

	override fun getName(index: Int) = FLAG_NAMES[index]

	override operator fun set(index: Int, value: Boolean) {
		// Bits 0-3 cannot be set
		if (index < 4) return
		flag[index] = value
	}

	/**
	 * Sets flags with a single byte
	 */
	override fun set(value: Int) {
		flag[7] = value and 0x80 != 0
		flag[6] = value and 0x40 != 0
		flag[5] = value and 0x20 != 0
		flag[4] = value and 0x10 != 0
	}
}
