package com.arcnor.anouckmse.machine.gameboy

import com.arcnor.cpu.Register
import com.arcnor.system.MemoryChip
import java.io.File
import java.util.Arrays

class GBMemory(rom: ByteArray?) : MemoryChip {
	companion object {
		const val REG_ROM0 = 0
		const val REG_ROMX = 1
		const val REG_VRAM = 2
		const val REG_SRAM = 3
		const val REG_WRAM0 = 4
		const val REG_WRAMX = 5
		const val REG_ECHO = 6
		const val REG_OAM = 7
		const val REG_UNUSED = 8
		const val REG_IO = 9
		const val REG_HRAM = 10
		const val REG_IER = 11
	}

	private val mem = ByteArray(getTotalSize())

	init {
		val bootRom = File("bios/GB/DMG_ROM.bin").readBytes()
		System.arraycopy(bootRom, 0, mem, 0, bootRom.size)
	}

	// FIXME: The reading is a bit more complex than direct access
	override fun readSigned(addr: Int) = mem[addr]

	// FIXME: The reading is a bit more complex than direct access
	override fun readUnsigned(addr: Int) = readSigned(addr).toInt() and 0xFF

	override fun readSigned(addr: Register) = readSigned(addr.get())

	override fun readUnsigned(addr: Register) = readUnsigned(addr.get())

	override fun write(addr: Int, value: Byte) {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	// FIXME: The reading is a bit more complex than direct access
	override fun getData(addr: Int, length: Int) = Arrays.copyOfRange(mem, addr, addr + length)

	override val regionNames = arrayOf(
			"ROM0", //"ROM Bank 0",
			"ROMX", //"ROM Bank X (Switchable)",
			"VRAM",
			"SRAM", //"Ext RAM",
			"WRAM0", //"RAM Bank 0",
			"WRAMX", //"RAM Bank X",
			"ECHO", //"RAM Bank 1 (Mirror)",
			"OAM", //"Sprite RAM",
			"UNUSED",
			"I/O",
			"HRAM", //"High RAM",
			"IE Register" // "Interrupt Enable Register"
	)

	override fun getRegionLength(region: Int) = when (region) {
		REG_ROM0, REG_ROMX -> 0x4000L
		REG_VRAM, REG_SRAM -> 0x2000L
		REG_WRAM0, REG_WRAMX -> 0x1000L
		REG_ECHO -> 0x1E00L
		REG_OAM -> 160L
		REG_UNUSED -> 0x60L
		REG_IO -> 0x80L
		REG_HRAM -> 0x7FL
		REG_IER -> 1L
		else -> throw RuntimeException("Invalid register")
	}

	override fun getRegionOffset(region: Int, offset: Long) = when (region) {
		REG_ROM0 -> 0x0000L
		REG_ROMX -> 0x4000L
		REG_VRAM -> 0x8000L
		REG_SRAM -> 0xA000L
		REG_WRAM0 -> 0xC000L
		REG_WRAMX -> 0xD000L
		REG_ECHO -> 0xE000L
		REG_OAM -> 0xFE00L
		REG_UNUSED -> 0xFEA0L
		REG_IO -> 0xFF00L
		REG_HRAM -> 0xFF80L
		REG_IER -> 0xFFFFL
		else -> throw RuntimeException("Invalid register")
	}

	override fun getTotalSize() = 0x10000
}