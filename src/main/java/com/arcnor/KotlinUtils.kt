package com.arcnor

fun Boolean.toInt() = if (this) 1 else 0
