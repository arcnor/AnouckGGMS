package com.arcnor.anouckmse.machine.nes

import com.arcnor.cpu.Register
import com.arcnor.system.MemoryChip
import java.io.ByteArrayInputStream
import java.io.DataInputStream
import java.io.IOException
import java.util.Arrays

@Suppress("OVERRIDE_BY_INLINE", "NOTHING_TO_INLINE")
/**
 * @param rom ROM in iNES format
 */
class NESMemory(rom: ByteArray) : MemoryChip {
	private val trainer: ByteArray?
	private val prgROM: ByteArray
	private val prgRAM: ByteArray
	private val chrROM: ByteArray
	private val RAM = ByteArray(0x800)
	private val SRAM = ByteArray(0x2000)
	private val regs = ByteArray(8)
	private val regs2 = ByteArray(0x20)

	init {
		val dis = DataInputStream(ByteArrayInputStream(rom))
		var tempTrainer: ByteArray? = null
		val tempPRGROM: ByteArray
		val tempPRGRAM: ByteArray
		val tempCHRROM: ByteArray

		try {
			if (dis.readByte() != 'N'.toByte() || dis.readByte() != 'E'.toByte() || dis.readByte() != 'S'.toByte() || dis.readByte().toInt() != 0x1A) {
				throw RuntimeException("Invalid iNES ROM")
			}
			val n16Bnks = dis.readByte().toInt()
			val n8VBnks = dis.readByte().toInt()

			val flags6 = dis.readByte().toInt()
			val vertMirror = flags6 and 0x01 != 0
			val battBacked = flags6 and 0x02 != 0
			val hasTrainer = flags6 and 0x04 != 0
			val fourScreen = flags6 and 0x08 != 0
			val flags7 = dis.readByte().toInt()
			val vsSystem = flags7 and 0x01 != 0
			val mapper = flags7 and 0xF0 or (flags6 and 0xF0 shr 4)

			val n8Bnks = dis.readByte().toInt()
			val flag9 = dis.readByte().toInt()
			val flag10 = dis.readByte().toInt()
			for (j in 0..4) {
				dis.readByte()
			}

			if (hasTrainer) {
				tempTrainer = ByteArray(512)
				dis.read(tempTrainer)
			}
			tempPRGROM = ByteArray(0x4000 * if (n16Bnks > 1) n16Bnks else 2)
			dis.read(tempPRGROM, 0, 0x4000 * n16Bnks)
			if (n16Bnks == 1) {
				System.arraycopy(tempPRGROM, 0, tempPRGROM, 0x4000, 0x4000)
			}
			tempCHRROM = ByteArray(0x1000 * if (n8VBnks > 0) n8VBnks else 1)
			dis.read(tempCHRROM)
			tempPRGRAM = ByteArray(0x1000 * if (n8Bnks > 0) n8Bnks else 1)
		} catch (e: IOException) {
			e.printStackTrace()

			throw RuntimeException("Cannot initialize")
		}

		trainer = tempTrainer
		prgROM = tempPRGROM
		chrROM = tempCHRROM
		prgRAM = tempPRGRAM
	}

	override fun readSigned(addr: Int) = when {
		addr < 0x2000 -> RAM[addr and 0x07FF]
		addr < 0x4000 -> regs[addr and 0x0007]      // I/O registers
		addr < 0x4020 -> regs2[addr and 0x001F]     // I/O registers
		addr < 0x6000 -> 0                          // Expansion ROM
		addr < 0x8000 -> SRAM[addr - 0x6000]
		addr < 0x10000 -> prgROM[addr - 0x8000]
		else -> 0
	}

	override fun readUnsigned(addr: Int) = readSigned(addr).toInt() and 0xFF

	override inline fun readSigned(addr: Register) = readSigned(addr.get())
	override inline fun readUnsigned(addr: Register) = readUnsigned(addr.get())

	override fun write(addr: Int, value: Byte) {
		when {
			addr < 0x2000 -> RAM[addr and 0x07FF] = value
			addr < 0x4000 -> regs[addr and 0x0007] = value      // I/O registers
			addr < 0x4020 -> regs2[addr and 0x001F] = value     // I/O registers
			addr < 0x6000 -> {}                                 // Expansion ROM
			addr < 0x8000 -> SRAM[addr - 0x6000] = value
			addr < 0x10000 -> prgROM[addr - 0x8000] = value
		}
	}

	override fun getData(addr: Int, length: Int): ByteArray? {
		when {
			addr < 0x2000 -> {
				val newAddr = addr and 0x07FF
				return Arrays.copyOfRange(RAM, newAddr, newAddr + length)
			}
			addr < 0x4000 -> {
				val newAddr = addr and 0x0007    // I/O registers
				return Arrays.copyOfRange(regs, newAddr, newAddr + length)
			}
			addr < 0x4020 -> {
				val newAddr = addr and 0x001F    // I/O registers
				return Arrays.copyOfRange(regs2, newAddr, newAddr + length)
			}
			addr < 0x6000 -> {    // Expansion ROM
			}
			addr < 0x8000 -> {
				val newAddr = addr - 0x6000
				return Arrays.copyOfRange(SRAM, newAddr, newAddr + length)
			}
			addr < 0x10000 -> {
				val newAddr = addr - 0x8000
				return Arrays.copyOfRange(prgROM, newAddr, newAddr + length)
			}
		}
		return ByteArray(length)
	}

	override val regionNames = arrayOf("ZERO", "STACK", "RAM", "REGS", "REGS2", "CARTRAM", "LROM", "HROM")

	override fun getRegionLength(region: Int) = when (region) {
		0 -> 0x0100L
		1 -> 0x0100L
		2 -> 0x0600L
		3 -> regs.size.toLong()
		4 -> regs2.size.toLong()
		5 -> 0x2000L
		6 -> 0x4000L
		7 -> 0x4000L
		else -> -1L
	}

	override fun getRegionOffset(region: Int, offset: Long) = when (region) {
		0 -> 0x0000L
		1 -> 0x0100L
		2 -> 0x0200L
		3 -> 0x2000L
		4 -> 0x4000L
		5 -> 0x6000L
		6 -> 0x8000L
		7 -> 0xC000L
		else -> -1L
	}

	override fun getTotalSize() = 0x10000
}
