package com.arcnor.anouckmse.debugger

import com.arcnor.anouckmse.swing.MutableListModel
import java.util.ArrayList
import javax.swing.AbstractListModel

internal class BreakpointListModel<T : Comparable<T>> : AbstractListModel<T>(), MutableListModel<T> {
	private val breakpoints = ArrayList<T>()

	override fun getSize() = breakpoints.size

	override fun getElementAt(index: Int) = breakpoints[index]

	override fun add(obj: T): Boolean {
		if (breakpoints.contains(obj)) {
			return false
		}
		val result = breakpoints.add(obj)
		if (result) {
			breakpoints.sort()
			val pos = breakpoints.indexOf(obj)
			fireIntervalAdded(this, pos, pos)
		}
		return result
	}

	override fun contains(obj: T) = breakpoints.contains(obj)

	override fun remove(obj: T): Boolean {
		val idx = breakpoints.indexOf(obj)
		val result = breakpoints.remove(obj)
		if (result) {
			fireIntervalRemoved(this, idx, idx)
		}
		return result
	}

	override fun remove(index: Int): Boolean {
		val result = breakpoints.removeAt(index)
		if (result != null) {
			fireIntervalRemoved(this, index, index)
		}
		return result != null
	}
}
